<?php
	function optionsframework_option_name()
	{
		return 'options-framework-theme';
	}

	function optionsframework_options()
	{
		// Pull all the pages into an array
		$options_pages 		= array();
		$options_pages_obj 	= get_pages( 'sort_column=post_parent,menu_order' );
		$options_pages[''] 	= 'Select a page:';
		
		foreach ($options_pages_obj as $page)
		{
			$options_pages[$page->ID] = $page->post_title;
		}

		// If using image radio buttons, define a directory path
		$imagepath =  get_template_directory_uri() . '/images/';

		$options = array();


			/* Start opciones Branding */
			$options[] = array(
				'name' => __( 'Branding', 'theme-textdomain' ),
				'type' => 'heading'
			);
				/* Personalizar logo */
				$options[] = array(
					'name' => __( 'Agregar Logo', 'theme-textdomain' ),
					'desc' => __( 'Para una buena relacion de aspecto la imagen debe ser proporcional 
									entre 350 X 46 pixeles.', 'theme-textdomain' ),
					'id' => 'logo',
					'type' => 'upload'
				);

				/* Personalizar favicon */
				$options[] = array(
					'name' => __( 'Agregar Favicon', 'theme-textdomain' ),
					'desc' => __( 'Para una buena relacion de aspecto la imagen debe ser proporcional 
									entre 50 X 50 pixeles.', 'theme-textdomain' ),
					'id' => 'favicon',
					'type' => 'upload'
				);

				/* Personalizar titulo */
				$options[] = array(
					'name' => __( 'Titulo Web'),
					'desc' => __( 'Este titulo es interpretado por el navegador'),
					'id' => 'titulo',
					'placeholder' => 'Titulo Web',
					'std' => 'One Focus Digital',
					'type' => 'text'
				);

				/* Personalizar teléfono */
				$options[] = array(
					'name' => __( 'Teléfono Label'),
					'desc' => __( 'Este es el teléfono de la compañía'),
					'id' => 'telefono',
					'placeholder' => 'Teléfono',
					'std' => '(+1) 786 355 4444',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Teléfono Link'),
					'desc' => __( 'Este es el link que se activará al presionar el teléfono'),
					'id' => 'telefono-link',
					'placeholder' => 'Teléfono Link',
					'std' => '+17863554444',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Email'),
					'desc' => __( 'Este es el Email de la compañía'),
					'id' => 'email-link',
					'placeholder' => 'Email Link',
					'std' => 'info@onefocusdigital.com',
					'type' => 'text'
				);
			/* End opciones Branding */



			/* Start opciones Banner */
			$options[] = array(
				'name' => __( 'Menu', 'theme-textdomain' ),
				'type' => 'heading'
			);
				/* Strat Opciones del menú */
				$options[] = array(
					'name' => __( 'Link 1'),
					'desc' => __( 'Link Bienvenido'),
					'id' => 'bienvenido',
					'placeholder' => 'Bienvenido',
					'std' => 'Bienvenido',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Link 2'),
					'desc' => __( 'Link ¿Que Hacemos?'),
					'id' => 'que_hacemos',
					'placeholder' => '¿Que Hacemos?',
					'std' => '¿Que Hacemos?',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Link 3'),
					'desc' => __( 'Link ¿Como trabajamos?'),
					'id' => 'como_trabajamos',
					'placeholder' => '¿Como trabajamos?',
					'std' => '¿Como trabajamos?',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Link 4'),
					'desc' => __( 'Link Proyectos'),
					'id' => 'proyectos',
					'placeholder' => 'Proyectos',
					'std' => 'Proyectos',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Link 5'),
					'desc' => __( 'Link Contactanos'),
					'id' => 'contactanos',
					'placeholder' => 'Contactanos',
					'std' => 'Contactanos',
					'type' => 'text'
				);
				/* End Opciones del menú */

			/* End opciones Banner */

			
			/* Start opciones Banner */
			$options[] = array(
				'name' => __( 'Bienvenido', 'theme-textdomain' ),
				'type' => 'heading'
			);	
				/* Start opciones Bienvenido */

				/* Personalizar titulo del banner */
				$options[] = array(
					'name' => __( 'Titulo Banner Bienvenido'),
					'desc' => __( 'Este el titulo informativo del banner'),
					'id' => 'titulo-bienvenido',
					'placeholder' => 'Agencia de Publicidad, Diseño & Web.',
					'std' => 'Agencia de Publicidad, Diseño & Web.',
					'type' => 'text'
				);

				/* Personalizar imagen del banner */
				$options[] = array(
					'name' => __( 'Imágen Banner Bienvenido'),
					'desc' => __( 'Imágen que se mestra en el banner de bienvenida', 'theme-textdomain' ),
					'id' => 'imagen-bienvenido',
					'type' => 'upload'
				);

				/* Personalizar contenido del banner */
				$options[] = array(
					'name' => __( 'Contenido Banner Bienvenido'),
					'desc' => __( 'Este el titulo informativo del banner'),
					'id' => 'contenido-bienvenido',
					'placeholder' => 'Contenido Banner Bienvenido',
					'std' => 'One Focus Digital es un equipo estratégico, proveedor de servicios online y offline, realizamos consultoría personalizada, Software, ERP, CRM, E-commerce, Social Media, SEO, SEM entre otras oportunidades personalizadas.',
					'type' => 'editor'
				);
				
				/* End opciones Bienvenido */
			/* End opciones Banner */


			/* Start opciones ¿Que Hacemos? */
			$options[] = array(
				'name' => __( 'Que Hacemos', 'theme-textdomain' ),
				'type' => 'heading'
			);
				/* Start opciones ¿Que Hacemos? */
				$options[] = array(
					'name' => __( 'Titulo Que Hacemos'),
					'desc' => __( 'Este el titulo informativo del apartado Que Hacemos'),
					'id' => 'titulo-que-hacemos',
					'placeholder' => 'QUE HACEMOS, MÁS FACIL ¡IMPOSIBLE!',
					'std' => 'QUE HACEMOS, MÁS FACIL ¡IMPOSIBLE!',
					'type' => 'text'
				);

				/* Start Servicios */

				/* Personalizar servicio 1 */
				$options[] = array(
					'name' => __( 'Titulo Servicio 1'),
					'desc' => __( 'Este el titulo del servicio 1'),
					'id' => 'titulo-servicio1',
					'placeholder' => 'Branding',
					'std' => 'Branding',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Contenido Servicio 1'),
					'desc' => __( 'Este el contenido del servicio 1'),
					'id' => 'contenido-servicio1',
					'placeholder' => 'Agencia de Publicidad, Diseño & Web.Desarrollamos todos los atributos, tanto tangibles como intangibles, que crean la imagen de una empresa, producto y/o servicio. Cambiamos lo que es por lo que queremos que sea.',
					'std' => 'Agencia de Publicidad, Diseño & Web.Desarrollamos todos los atributos, tanto tangibles como intangibles, que crean la imagen de una empresa, producto y/o servicio. Cambiamos lo que es por lo que queremos que sea.',
					'type' => 'textarea'
				);
				/* Personalizar servicio 2 */
				$options[] = array(
					'name' => __( 'Titulo Servicio 2'),
					'desc' => __( 'Este el titulo del servicio 2'),
					'id' => 'titulo-servicio2',
					'placeholder' => 'Copy Write',
					'std' => 'Copy Write',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Contenido Servicio 2'),
					'desc' => __( 'Este el titulo del servicio 2'),
					'id' => 'contenido-servicio2',
					'placeholder' => 'Nuestra especialidad es atraer la atención, aportar ideas creativas, concepciones estratégicas, arquitectura de información, diseños frescos y mensajes de un elevedo nivel de innovación.',
					'std' => 'Nuestra especialidad es atraer la atención, aportar ideas creativas, concepciones estratégicas, arquitectura de información, diseños frescos y mensajes de un elevedo nivel de innovación.',
					'type' => 'textarea'
				);
				/* Personalizar servicio 3 */
				$options[] = array(
					'name' => __( 'Titulo Servicio 3'),
					'desc' => __( 'Este el titulo del servicio 3'),
					'id' => 'titulo-servicio3',
					'placeholder' => 'ROI',
					'std' => 'ROI',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Contenido Servicio 3'),
					'desc' => __( 'Este el contenido del servicio 3'),
					'id' => 'contenido-servicio3',
					'placeholder' => 'Al final del día, un impacto positivo en tu balance económico es lo que nos motiva.',
					'std' => 'Al final del día, un impacto positivo en tu balance económico es lo que nos motiva.',
					'type' => 'textarea'
				);

				/* End Servicios */
				
			/* End opciones ¿Que Hacemos? */


			/* Start opciones ¿Como Trabajamos? */
			$options[] = array(
				'name' => __( 'Como Trabajamos', 'theme-textdomain' ),
				'type' => 'heading'
			);
				$options[] = array(
					'name' => __( 'Titulo Como Trabajamos'),
					'desc' => __( 'Este el titulo informativo del apartado Como Trabajamos'),
					'id' => 'titulo-como-trabajamos',
					'placeholder' => 'LA INNOVACION SE PUEDE AMPLIFICAR A TRAVÉS DE ASOCIACIONES ESTRATÉGICAS',
					'std' => 'LA INNOVACION SE PUEDE AMPLIFICAR A TRAVÉS DE ASOCIACIONES ESTRATÉGICAS',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Imágen Estrategias'),
					'desc' => __( 'Este la imágen del apartado Como Trabajamos'),
					'id' => 'imagen-estrategias',
					'type' => 'upload'
				);

				/* Star personalizacion de estrategias */
				$options[] = array(
					'name' => __( 'Titulo Estrategia 1'),
					'desc' => __( 'Este el titulo de la estrategia 1'),
					'id' => 'titulo-estrategia1',
					'placeholder' => 'Planificamos',
					'std' => 'Planificamos',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Contenido Estrategia 1 '),
					'desc' => __( 'Este el contenido de la estrategia 1'),
					'id' => 'contenido-estrategia1',
					'placeholder' => 'Una entrvista inicial, ya sea personal, por teléfono o vía Skype, totalmente personalizada.',
					'std' => 'Una entrvista inicial, ya sea personal, por teléfono o vía Skype, totalmente personalizada.',
					'type' => 'textarea'
				);
				$options[] = array(
					'name' => __( 'Titulo Estrategia 2'),
					'desc' => __( 'Este el titulo de la estrategia 2'),
					'id' => 'titulo-estrategia2',
					'placeholder' => 'Hablamos',
					'std' => 'Hablamos',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Contenido Estrategia 2'),
					'desc' => __( 'Este el contenido de la estrategia 2'),
					'id' => 'contenido-estrategia2',
					'placeholder' => 'De las estrategias de marketing particulares para cada cliente sean mayoristas o minoristas.',
					'std' => 'De las estrategias de marketing particulares para cada cliente sean mayoristas o minoristas.',
					'type' => 'textarea'
				);
				$options[] = array(
					'name' => __( 'Titulo Estrategia 3'),
					'desc' => __( 'Este el titulo de la estrategia 3'),
					'id' => 'titulo-estrategia3',
					'placeholder' => 'Pensamos',
					'std' => 'Pensamos',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Contenido Estrategia 3'),
					'desc' => __( 'Este el contenido de la estrategia 3'),
					'id' => 'contenido-estrategia3',
					'placeholder' => 'Que, concreatar una venta es importante pero lograr la fidelidad de los clientes en vital.',
					'std' => 'Que, concreatar una venta es importante pero lograr la fidelidad de los clientes en vital.',
					'type' => 'textarea'
				);
				$options[] = array(
					'name' => __( 'Titulo Estrategia 4'),
					'desc' => __( 'Este el titulo de la estrategia 4'),
					'id' => 'titulo-estrategia4',
					'placeholder' => 'Creamos',
					'std' => 'Creamos',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Contenido Estrategia 4 '),
					'desc' => __( 'Este el contenido de la estrategia 4'),
					'id' => 'contenido-estrategia4',
					'placeholder' => 'Estrategias de valor que posicionan tu marca y mejoran la reantabilidad de tu negocio.',
					'std' => 'Estrategias de valor que posicionan tu marca y mejoran la reantabilidad de tu negocio.',
					'type' => 'textarea'
				);
				$options[] = array(
					'name' => __( 'Titulo Estrategia 5'),
					'desc' => __( 'Este el titulo de la estrategia 5'),
					'id' => 'titulo-estrategia5',
					'placeholder' => 'Comunicamos',
					'std' => 'Comunicamos',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Contenido Estrategia 5'),
					'desc' => __( 'Este el contenido de la estrategia 5'),
					'id' => 'contenido-estrategia5',
					'placeholder' => 'De forma innovadora con tu público objetivo, generando un impacto positivo y diferente.',
					'std' => 'De forma innovadora con tu público objetivo, generando un impacto positivo y diferente.',
					'type' => 'textarea'
				);
				$options[] = array(
					'name' => __( 'Titulo Estrategia 6'),
					'desc' => __( 'Este el titulo de la estrategia 6'),
					'id' => 'titulo-estrategia6',
					'placeholder' => 'Crecemos',
					'std' => 'Crecemos',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Contenido Estrategia 6'),
					'desc' => __( 'Este el contenido de la estrategia 6'),
					'id' => 'contenido-estrategia6',
					'placeholder' => 'como un equipo multidisciplinario enfocados en impulsar mejores negocios con nuestros clientes.',
					'std' => 'como un equipo multidisciplinario enfocados en impulsar mejores negocios con nuestros clientes.',
					'type' => 'textarea'
				);
				/* End personalizacion de estrategias */

			/* End opciones ¿Como Trabajamos? */


			/* Start opciones Proyectos */
			$options[] = array(
				'name' => __( 'Proyectos', 'theme-textdomain' ),
				'type' => 'heading'
			);
				/* Start opciones Proyectos */
				$options[] = array(
					'name' => __( 'Titulo Proyectos'),
					'desc' => __( 'Este el titulo informativo del apartado Proyectos'),
					'id' => 'titulo-proyectos',
					'placeholder' => 'PROYECTOS',
					'std' => 'PROYECTOS',
					'type' => 'text'
				);
				/* End opciones Proyectos */
			/* End opciones Proyectos */


			/* Start opciones Contactanos */
			$options[] = array(
				'name' => __( 'Contactanos', 'theme-textdomain' ),
				'type' => 'heading'
			);
				$options[] = array(
					'name' => __( 'Titulo Contactanos'),
					'desc' => __( 'Este el titulo informativo del apartado Contactanos'),
					'id' => 'titulo-contactanos',
					'placeholder' => 'LA PRIMERA CONSULTA ES GRATIS!',
					'std' => 'LA PRIMERA CONSULTA ES GRATIS!',
					'type' => 'text'
				);

				$options[] = array(
					'name' => __( 'Titulo Informativo'),
					'desc' => __( 'Este el titulo informativo del formulario de Contacto'),
					'id' => 'titulo-formulario',
					'placeholder' => '¿TIENES PREGUNTAS?',
					'std' => '¿TIENES PREGUNTAS?',
					'type' => 'text'
				);
				$options[] = array(
					'name' => __( 'Descripcion Informativo'),
					'desc' => __( 'Este la descripcion del formulario de Contacto'),
					'id' => 'descripcion-formulario',
					'placeholder' => 'Que vamos a lograr?, Como la vamos a hacer?, Que tiempo va a tardar?, Cuánto va a costar?, Cuáles son las forma de pago?, Cuáles son las metas y objetivos iniciales?, responderemos a todas tus preguntas...',
					'std' => 'Que vamos a lograr?, Como la vamos a hacer?, Que tiempo va a tardar?, Cuánto va a costar?, Cuáles son las forma de pago?, Cuáles son las metas y objetivos iniciales?, responderemos a todas tus preguntas...',
					'type' => 'textarea'
				);
			/* End opciones Contactanos */




		
		



































		return $options;
	}