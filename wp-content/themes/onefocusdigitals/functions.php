<?php
	function url_assets($recurso = '')
	{
		return site_url('/wp-content/themes/onefocusdigitals/assets/'.$recurso);
	}

	/*-----------------------------------------*/
	/* Cargar Panle de Opciones
	/*-----------------------------------------*/
	if ( !function_exists( 'optionsframework_init' ) ) {
		define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
		require_once dirname( __FILE__ ) . '/inc/options-framework.php';
	}
	$options = get_option('options-framework-theme');



	add_theme_support( 'post-thumbnails' ); 
	function get_portfolio($limit = 6){

		$args=array(
			'post_type' 		=> 'portfolio',
			'post_status' 		=> 'publish',
			'posts_per_page' 	=> -1,
			'caller_get_posts'	=> $limit);

		$my_query = null;
		$my_query = New WP_Query($args);

		if( $my_query->have_posts() ) {?>
		<style>
			.img-project{
				position:relative;
				width:250px;
				height:250px;
			}
			.img-project img{
				position:absolute;
				width:100%;
				height:100%;
				top:0;
				left:0;
				z-index:2;
			}
			.img-project img:hover{
				z-index:1;
			}
			.texto:hover{
				z-index:2;
			}
			.texto{
				position:absolute;
				width:100%;
				height:100%;
				top:0;
				left:0;
				z-index:1;
				margin-top: 0px;
				background: rgba(0,0,0,0.8);
				text-align: center;
				padding-top: 40%;
			}
		</style>
		<?php if (count($my_query->get_posts())>9) { ?>
			<div role="toolbar" class="wpsisac-slick-slider-1 wpsisac-slick-slider design-5  slick-slider">
					<?php $i=0;?>
					<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
							<?php $i++;// link -> the_permalink() ?>
							<?php if ($i==1) {?>
							<div>
							<?php } ?>
								<div class="col-md-4 col-sm-6 col-xs-12 col-lg-4 project">
									<div class="img-project">
										<?php echo the_post_thumbnail(); ?>
										<h4 class="texto color-blue-dark">
											<a href="<?php echo the_permalink() ?>">
												<?php the_title(); ?>
											</a>
										</h4>
									</div>
								</div>
							<?php if ($i%9==0 ) {?>
							</div>
							<div>
							<?php } ?>
							<?php if ($i%9==0) {?>
							
							<?php } ?>
					<?php endwhile;?>
				</div>
			</div>
		<?php }	?>

		<?php if (count($my_query->get_posts())<=9) { ?>
			<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
						<div class="col-md-4 col-sm-6 col-xs-12 col-lg-4 project">
							<div class="img-project">
								<?php echo the_post_thumbnail(); ?>
								<h4 class="texto color-blue-dark">
									<a href="<?php echo the_permalink() ?>">
										<?php the_title(); ?>
									</a>
								</h4>
							</div>
						</div>
			<?php endwhile;?>
		<?php } ?>	
		<?php
		}
		wp_reset_query();
	}

?>
