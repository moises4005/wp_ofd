	<!-- Start Row 3 [servicios] -->
	<div id="que-hacemos" class="row-fluid row_3">
		
		<!-- title services -->
		<div class="background-blue-dark">
			<div class="container title-services text-center color-white">
				<h3><?php echo $options['titulo-que-hacemos']; ?></h3>
			</div>
		</div>

		<div class="container services">
			
			<!-- services 1 -->
			<div class="col-md-4">
				<div class="service text-center">
					<div class="img-services">
						<i class="fa fa-tag fa-3x background-red-light color-white"></i>
					</div>
					<div class="title-services"><?php echo $options['titulo-servicio1']; ?></div>
					<div class="content-services">
						Desarrollamos todos los atributos,
						tanto tangibles como intangibles,
						que crean la imagen de una empresa,
						producto y/o servicio. Cambiamos
						lo que es por lo que queremos que sea.

					</div>
				</div>
			</div>

			<!-- services 2 -->
			<div class="col-md-4">
				<div class="service text-center">
					<div class="img-services">
						<i class="fa fa-rocket fa-3x background-red-light color-white"></i>
					</div>
					<div class="title-services"><?php echo $options['titulo-servicio2']; ?></div>
					<div class="content-services">
						Nuestra especialidad es atraer la atención,
						aportar ideas creativas, concepciones estratégicas,
						arquitectura de información, diseños frescos y
						mensajes de un elevedo nivel de innovación.
					</div>
				</div>
			</div>

			<!-- services 3 -->
			<div class="col-md-4">
				<div class="service text-center">
					<div class="img-services">
						<i class="fa fa-line-chart fa-3x background-red-light color-white"></i>
					</div>
					<div class="title-services"><?php echo $options['titulo-servicio3']; ?></div>
					<div class="content-services">
						Al final del día, un impacto positivo
						en tu balance económico es lo que nos
						motiva.
						<br>
						Nos convertimos en tu aliado, construyendo
						juntos el éxito de tu idea o negocio actual.
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- End Row 3 -->