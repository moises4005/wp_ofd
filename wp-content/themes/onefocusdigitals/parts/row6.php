	<!-- Start Row 6 [contact] -->
	<div id="contactanos" class="row-fluid row_6">

		<!-- title contact -->
		<div class="background-blue-dark">
			<div class="container title-contact text-center color-white">
				<h3><?php echo $options['titulo-contactanos']; ?></h3>
			</div>
		</div>

		<div class="container contact">
			

			<!-- Texto de contacto -->
			<div class="col-md-6">
				<div class="col-md-10">
					<h4>¿TIENES PREGUNTAS?</h4>

					<p>
						Que vamos a lograr?, Como la vamos a hacer?,
						Que tiempo va a tardar?, Cuánto va a costar?,
						Cuáles son las forma de pago?, Cuáles son las
						metas y objetivos iniciales?, responderemos
						a todas tus preguntas...
					</p>

					<p>
						Llámanos para mas información al:
					</p>

					<a class="phone" href="tel:<?php echo $options['telefono-link']; ?>">
						<i class="fa fa-phone"></i>
						<?php echo $options['telefono']; ?>
					</a>
				</div>
			</div>
			
			<!-- Formulario de contacto -->
			<div class="col-md-6">
				<div class="form">
					<?php echo do_shortcode( '[contact-form-7 id="100" title="Formulario de contacto 1"]' ); ?>
				</div>
			</div>

		</div>
	</div>
	<!-- End Row 6 -->
	