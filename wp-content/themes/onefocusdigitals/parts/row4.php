	<!-- Start Row 4 [pasos] -->
	<div id="como-trabajamos" class="row-fluid row_4">

		<!-- title steps -->
		<div class="background-blue-dark">
			<div class="container title-steps text-center color-white">
				<h3><?php echo $options['titulo-como-trabajamos']; ?></h3>
			</div>
		</div>


		<div class="container steps">

			<div class="col-md-6">

				<?php while($stop == 0): $i++; ?>

				<?php if( isset($options['titulo-estrategia'.$i]) ==TRUE ) { ?>

				<!-- step 1 -->
				<div class="step">

					<div class="col-md-2 icon-step">
						<span class="numero-steep"><?php echo $i ?></span>
					</div>

					<div class="col-md-10">
						<div class="">
							<div class="title-step-1"><h4><?php echo $options['titulo-estrategia'.$i]; ?></h4></div>
						</div>
						<div class="">
							<div class="content-step-1"><?php echo $options['contenido-estrategia'.$i]; ?></div>
						</div>
					</div>

				</div>

				<?php } else { $stop = 1; } ?>
				<?php endwhile; ?>

			</div>

			<!-- start imagen idea -->
			<div class="col-md-6 imagen-idea hidden-xs hidden-sm text-center">
				<img src="<?php echo $options['imagen-estrategias']; ?>" class="img-responsive center-block" alt="Metodología One Focus Digital">
			</div>
			<!-- end imagen idea -->

		</div>
	</div>
	<!-- End Row 4 -->