<head>

	<!-- BASIC PAGE NEEDS
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php echo $options['titulo']; ?></title>

	<!-- MOBILE SPECIFIC METAS
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- PARAMETROS PINGBACK DE WORDPRESS
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>

	<!-- FONT
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

	<!-- CSS
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="stylesheet" href="<?php echo url_assets('css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo url_assets('css/bootstrap-theme.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo url_assets('css/font-awesome.min.css'); ?>">
	<link  rel="stylesheet" type="text/css" href="<?php echo url_assets('../../../plugins/wp-slick-slider-and-image-carousel/assets/css/slick.css');?>">
	<link  rel="stylesheet" type="text/css" href="<?php echo url_assets('../../../plugins/wp-slick-slider-and-image-carousel/assets/css/slick-slider-style.css');?>">
	<link rel="stylesheet" href="<?php echo bloginfo('stylesheet_url'); ?>">


	<!-- FAVICON
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="<?php echo $options['favicon']; ?>">
	<script type="text/javascript" src="<?php echo url_assets('js/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo url_assets('js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo url_assets('../../../plugins/wp-slick-slider-and-image-carousel/assets/js/slick.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo url_assets('js/site.js'); ?>"></script>
</head>