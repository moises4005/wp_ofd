
<style type="text/css">

</style>
<nav class="navbar navbar-inverse row-new-1 navbar-fixed-top background-blue-dark color-white" role="navigation" style=''>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container">
	    <div class="navbar-header">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex6-collapse">
				<span class="sr-only">Desplegar navegación</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
		    </button>
		    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
		    	<img src="<?php echo $options['logo']; ?>" alt="One Focus Digital" class='logo img-responsive new-logo'>
		    </a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse navbar-ex6-collapse">
	      	<ul class="nav navbar-nav navbar-right" id="nav-top">
				<li class="">
					<a class="bienvenido hidden-xs link" href="#bienvenido"><?php echo $options['bienvenido']; ?></a>
				</li>
				<li class="">
					<a class="que-hacemos hidden-xs link" href="#que-hacemos"><?php echo $options['que_hacemos']; ?></a>
				</li>
				<li class="">
					<a class="como-trabajamos hidden-xs link" href="#como-trabajamos"><?php echo $options['como_trabajamos']; ?></a>
				</li>
				<li class="">
					<a class="proyectos hidden-xs link" href="#proyectos"><?php echo $options['proyectos']; ?></a> 
				</li>
				<li class="">
					<a class="contactanos hidden-xs link" href="#contactanos"><?php echo $options['contactanos']; ?></a>
				</li>
	      	</ul>
	      	<ul class="nav navbar-nav navbar-right visible-xs" id="nav-top2">

				<li class="">
					<a class="bienvenido link2" data-toggle="collapse" data-target=".navbar-collapse" href="#bienvenido"><?php echo $options['bienvenido']; ?></a>
				</li>
				<li class="">
					<a class="que-hacemos link2" data-toggle="collapse" data-target=".navbar-collapse" href="#que-hacemos"><?php echo $options['que_hacemos']; ?></a>
				</li>
				<li class="">
					<a class="como-trabajamos link2" data-toggle="collapse" data-target=".navbar-collapse" href="#como-trabajamos"><?php echo $options['como_trabajamos']; ?></a>
				</li>
				<li class="">
					<a class="proyectos link2" data-toggle="collapse" data-target=".navbar-collapse" href="#proyectos"><?php echo $options['proyectos']; ?></a> 
				</li>
				<li class="">
					<a class="contactanos link2" data-toggle="collapse" data-target=".navbar-collapse" href="#contactanos"><?php echo $options['contactanos']; ?></a>
				</li>
	      	</ul>
	    </div><!-- /.navbar-collapse -->
	   </div>
  </nav>

	