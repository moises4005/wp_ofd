	<!-- End Row 5 [Proyectos] -->
	<div id="proyectos" class="row-fluid row_5">

		<!-- title projects -->
		<div class="background-blue-dark">
			<div class="container title-projects text-center color-white">
				<h3><?php echo $options['titulo-proyectos']; ?></h3>
			</div>
		</div>

		<div class="container projects">
			<?php get_portfolio(); ?>
		</div>
	</div>
	<!-- End Row 5 -->