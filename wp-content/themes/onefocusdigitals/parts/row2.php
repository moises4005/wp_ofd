	<!-- Start Row 2 [banner] -->
	<div id="bienvenido" class="row-fluid row_2 background-red-dark">
		<div class="container banner color-white">
			
			<!-- Imágen banner -->
			<div class="col-md-4 text-center">
				<img src="<?php echo $options['imagen-bienvenido']; ?>" alt="One Focus Digital">
			</div>
			
			<div class="col-md-8">
				
				<!-- Titulo de banner -->
				<div class="title-banner">
					<h2 class="text-center"><?php echo $options['titulo-bienvenido']; ?></h2>
				</div>
				
				<!-- Contenido de banner -->
				<div class="contenido-banner text-center">
					<p>
						<?php echo $options['contenido-bienvenido']; ?>
					</p>

					<p>
						Llámanos ahora para mas información al <a class="phone color-white" href="tel:<?php echo $options['telefono-link']; ?>"><?php echo $options['telefono']; ?></a>
					</p>
				</div>

				<!-- Botones banner -->
				<div class="buttons-banner text-center">
					<a href="#contactanos" class="btn btn-default btn-lg active">
						<i class="fa fa-envelope-o"></i>
						ENVIANOS UN CORREO
					</a>
					<a href="#proyectos" class="btn btn-default btn-lg new-btn">

						<i class="fa fa-folder-o"></i>
						VER PROYECTOS
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- End Row 2 -->