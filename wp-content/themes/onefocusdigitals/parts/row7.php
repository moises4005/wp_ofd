	<!-- Start Row 7 [footer] -->
	<div class="row-fluid row_7 background-blue-dark color-white">
		<div class="container footer text-center">
			
			<!-- title web - sutitle web -->
			<h5>ONE FOCUS DIGITAL - AGENCIA DE PUBLICIDAD, SOFTWARE Y DISEÑO GRÁFICO</h5>

			<!-- copyright -->
			<p>Copyright © 2016 One Focus Digital - Todos los Dercheos Reservados.</p>
			
			<!-- links -->
			<a href="http://onefocusdigital.com/terminso-condiciones/">Términos y condiciones</a> | <a href="http://onefocusdigital.com/preguntas-frecuentes/">Preguntas frecuentes</a>

		</div>
	</div>
	
</body>
</html>