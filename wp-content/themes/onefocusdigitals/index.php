<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<?php include('parts/head.php'); ?>
<body  <?php body_class(); ?>>

	<?php include('parts/row1.php'); ?>
	<?php include('parts/row2.php'); ?>
	<?php include('parts/row3.php'); ?>
	<?php include('parts/row4.php'); ?>
	<?php include('parts/row5.php'); ?>
	<?php include('parts/row6.php'); ?>
	<?php include('parts/row7.php'); ?>

</body>
</html>