
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<?php include('parts/head.php'); ?>

<body <?php body_class(); ?>>
<nav class="navbar navbar-inverse row-new-1 navbar-fixed-top background-blue-dark color-white" role="navigation" style=''>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container">
	    <div class="navbar-header">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex6-collapse">
				<span class="sr-only">Desplegar navegación</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
		    </button>
		    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
		    	<img src="<?php echo $options['logo']; ?>" alt="One Focus Digital" class='logo img-responsive new-logo'>
		    </a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse navbar-ex6-collapse">
	      	<ul class="nav navbar-nav navbar-right" id="nav-top">
				<li class="">
					<a class="bienvenido hidden-xs link" href="<?php echo esc_url( home_url( '/' ) ); ?>/#bienvenido"><?php echo $options['bienvenido']; ?></a>
				</li>
				<li class="">
					<a class="que-hacemos hidden-xs link" href="<?php echo esc_url( home_url( '/' ) ); ?>/#que-hacemos"><?php echo $options['que_hacemos']; ?></a>
				</li>
				<li class="">
					<a class="como-trabajamos hidden-xs link" href="<?php echo esc_url( home_url( '/' ) ); ?>/#como-trabajamos"><?php echo $options['como_trabajamos']; ?></a>
				</li>
				<li class="">
					<a class="proyectos hidden-xs link" href="<?php echo esc_url( home_url( '/' ) ); ?>/#proyectos"><?php echo $options['proyectos']; ?></a> 
				</li>
				<li class="">
					<a class="contactanos hidden-xs link" href="<?php echo esc_url( home_url( '/' ) ); ?>/#contactanos"><?php echo $options['contactanos']; ?></a>
				</li>
	      	</ul>
	      	<ul class="nav navbar-nav navbar-right visible-xs" id="nav-top2">

				<li class="">
					<a class="bienvenido link2" data-toggle="collapse" data-target=".navbar-collapse" href="<?php echo esc_url( home_url( '/' ) ); ?>#bienvenido"><?php echo $options['bienvenido']; ?></a>
				</li>
				<li class="">
					<a class="que-hacemos link2" data-toggle="collapse" data-target=".navbar-collapse" href="<?php echo esc_url( home_url( '/' ) ); ?>/#que-hacemos"><?php echo $options['que_hacemos']; ?></a>
				</li>
				<li class="">
					<a class="como-trabajamos link2" data-toggle="collapse" data-target=".navbar-collapse" href="<?php echo esc_url( home_url( '/' ) ); ?>/#como-trabajamos"><?php echo $options['como_trabajamos']; ?></a>
				</li>
				<li class="">
					<a class="proyectos link2" data-toggle="collapse" data-target=".navbar-collapse" href="<?php echo esc_url( home_url( '/' ) ); ?>/#proyectos"><?php echo $options['proyectos']; ?></a> 
				</li>
				<li class="">
					<a class="contactanos link2" data-toggle="collapse" data-target=".navbar-collapse" href="<?php echo esc_url( home_url( '/' ) ); ?>/#contactanos"><?php echo $options['contactanos']; ?></a>
				</li>
	      	</ul>
	    </div><!-- /.navbar-collapse -->
   </div>
</nav>
<div id="bienvenido" class="row-fluid row_2 background-red-dark">
	<div class="container banner color-white">
		
		<div class="col-lg-12">
			<div class="page-header">
				<h2 class=""><?php the_title(); ?></h2>
			</div>
			<!-- Imágen banner -->
			<div class="row">
			<div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
				<div class="row">
    				<div class="col-xs-6 col-md-12 col-lg-12 col-xs-offset-4 col-lg-offset-1 col-md-offset-1 ">
						<div class="row">
							<a href="<?php the_permalink(); ?>" class="title-page">
							<?php  $default_attr = array('alt'	=> 'texto para mostrar en Alt',
										'title'	=> 'texto para mostrar en Title',
										'class' => 'img-responsive'
									);
								$size='full';
							 the_post_thumbnail( $size, $default_attr);
							?>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-sm-12 col-xs-12 col-lg-8">
				<!-- Titulo de banner -->
				
				<!-- Contenido de banner -->
				<div class="contenido-banner">
					<?php if(have_posts()): ?>
					<?php while(have_posts() ): the_post(); $i++; ?>
						<?php the_content();?>
					<?php endwhile; ?>
					<?php endif; ?>
				</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
<!-- End Row 2 -->
<?php include('parts/row5.php'); ?>
<?php include('parts/row6.php'); ?>
<?php include('parts/row7.php'); ?>
</body>
</html>
