<!DOCTYPE html>
<html lang="es">
<head>

	<!-- Basic Page Needs
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<meta charset="utf-8">
	<title>One Focus Digital</title>

	<!-- Mobile Specific Metas
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- FONT
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

	<!-- CSS
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="assets/css/style.css">

	<!-- Favicon
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="assets/images/favicon.png">
</head>
<body>

	<?php include('parts/row1.php'); ?>
	<?php include('parts/row2.php'); ?>
	<?php include('parts/row3.php'); ?>
	<?php include('parts/row4.php'); ?>
	<?php include('parts/row5.php'); ?>
	<?php include('parts/row6.php'); ?>
	<?php include('parts/row7.php'); ?>

</body>
<?php wp_enqueue_script( 'wp_enqueue_scripts');?>
</html>