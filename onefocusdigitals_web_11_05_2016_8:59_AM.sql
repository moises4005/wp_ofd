-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 11, 2016 at 08:30 AM
-- Server version: 5.5.46-0+deb8u1
-- PHP Version: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `onefocusdigitals_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
`meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE IF NOT EXISTS `wp_comments` (
`comment_ID` bigint(20) unsigned NOT NULL,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_crp_portfolios`
--

CREATE TABLE IF NOT EXISTS `wp_crp_portfolios` (
`id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `corder` text COLLATE utf8mb4_unicode_ci,
  `options` text COLLATE utf8mb4_unicode_ci,
  `extoptions` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_crp_portfolios`
--

INSERT INTO `wp_crp_portfolios` (`id`, `title`, `corder`, `options`, `extoptions`) VALUES
(1, 'DARENVIOS', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_crp_projects`
--

CREATE TABLE IF NOT EXISTS `wp_crp_projects` (
`id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci,
  `cover` text COLLATE utf8mb4_unicode_ci,
  `pics` text COLLATE utf8mb4_unicode_ci,
  `categories` text COLLATE utf8mb4_unicode_ci,
  `cdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_huge_itportfolio_images`
--

CREATE TABLE IF NOT EXISTS `wp_huge_itportfolio_images` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `portfolio_id` varchar(200) DEFAULT NULL,
  `description` text,
  `image_url` text,
  `sl_url` text,
  `sl_type` text NOT NULL,
  `link_target` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` tinyint(4) unsigned DEFAULT NULL,
  `published_in_sl_width` tinyint(4) unsigned DEFAULT NULL,
  `category` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_huge_itportfolio_images`
--

INSERT INTO `wp_huge_itportfolio_images` (`id`, `name`, `portfolio_id`, `description`, `image_url`, `sl_url`, `sl_type`, `link_target`, `ordering`, `published`, `published_in_sl_width`, `category`) VALUES
(1, 'Cutthroat & Cavalier', '1', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/1.jpg;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/1.1.jpg;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/1.2.jpg;', 'http://huge-it.com/wordpress-theme-company/', 'image', 'on', 0, 1, NULL, 'My First Category,My Third Category,'),
(2, 'Nespresso', '1', '<h6>Lorem Ipsum </h6><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><ul><li>lorem ipsum</li><li>dolor sit amet</li><li>lorem ipsum</li><li>dolor sit amet</li></ul>', 'https://vimeo.com/76602135;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/9.1.jpg;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/9.2.jpg;', 'http://huge-it.com/wordpress-theme-company/', 'video', 'on', 1, 1, NULL, 'My Second Category,'),
(3, 'Nexus', '1', '<h6>Lorem Ipsum </h6><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudexercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><ul><li>lorem ipsum</li><li>dolor sit amet</li><li>lorem ipsum</li><li>dolor sit amet</li></ul>', 'http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/3.jpg;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/3.1.jpg;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/3.2.jpg:https://www.youtube.com/watch?v=YMQdfGFK5XQ;', 'http://huge-it.com/wordpress-theme-company/', 'image', 'on', 2, 1, NULL, 'My Third Category,'),
(4, 'De7igner', '1', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><h7>Dolor sit amet</h7><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/4.jpg;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/4.1.jpg;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/4.2.jpg;', 'http://huge-it.com/portfolio-gallery/', 'image', 'on', 3, 1, NULL, 'My First Category,My Second Category,'),
(5, 'Autumn / Winter Collection', '1', '<h6>Lorem Ipsum</h6><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/2.jpg;', 'http://huge-it.com/portfolio-gallery/', 'image', 'on', 4, 1, NULL, 'My Second Category,My Third Category,'),
(6, 'Retro Headphones', '1', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/6.jpg;https://vimeo.com/80514062;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/6.1.jpg;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/6.2.jpg;', 'http://huge-it.com/portfolio-gallery/', 'image', 'on', 5, 1, NULL, 'My Third Category,'),
(7, 'Take Fight', '1', '<h6>Lorem Ipsum</h6><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><p>Excepteur sint occaecat cupidatat non proident , sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/7.jpg;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/7.2.jpg;https://www.youtube.com/watch?v=SP3Dgr9S4pM;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/7.3.jpg;', 'http://huge-it.com/portfolio-gallery/', 'image', 'on', 6, 1, NULL, 'My Second Category,'),
(8, 'The Optic', '1', '<h6>Lorem Ipsum </h6><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><ul><li>lorem ipsum</li><li>dolor sit amet</li><li>lorem ipsum</li><li>dolor sit amet</li></ul>', 'http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/8.jpg;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/8.1.jpg;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/8.3.jpg;', 'http://huge-it.com/wordpress-theme-company/', 'image', 'on', 7, 1, NULL, 'My First Category,'),
(9, 'Cone Music', '1', '<ul><li>lorem ipsumdolor sit amet</li><li>lorem ipsum dolor sit amet</li></ul><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/5.jpg;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/5.1.jpg;http://onefocusdigital.com/wp-content/plugins/portfolio-gallery/Front_images/projects/5.2.jpg;', 'http://huge-it.com/wordpress-theme-company/', 'image', 'on', 8, 1, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `wp_huge_itportfolio_portfolios`
--

CREATE TABLE IF NOT EXISTS `wp_huge_itportfolio_portfolios` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(200) NOT NULL,
  `sl_height` int(11) unsigned DEFAULT NULL,
  `sl_width` int(11) unsigned DEFAULT NULL,
  `pause_on_hover` text,
  `portfolio_list_effects_s` text,
  `description` text,
  `param` text,
  `sl_position` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` text,
  `categories` text NOT NULL,
  `ht_show_sorting` text NOT NULL,
  `ht_show_filtering` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_huge_itportfolio_portfolios`
--

INSERT INTO `wp_huge_itportfolio_portfolios` (`id`, `name`, `sl_height`, `sl_width`, `pause_on_hover`, `portfolio_list_effects_s`, `description`, `param`, `sl_position`, `ordering`, `published`, `categories`, `ht_show_sorting`, `ht_show_filtering`) VALUES
(1, 'My First Portfolio', 375, 600, 'on', '2', '4000', '1000', 'center', 1, '300', 'My First Category,My Second Category,My Third Category,', 'off', 'off');

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE IF NOT EXISTS `wp_links` (
`link_id` bigint(20) unsigned NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE IF NOT EXISTS `wp_options` (
`option_id` bigint(20) unsigned NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB AUTO_INCREMENT=567 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://onefocusdigital.com', 'yes'),
(2, 'home', 'http://onefocusdigital.com', 'yes'),
(3, 'blogname', 'OneFocusDigital', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'irwing@onefocusdigital.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'hack_file', '0', 'yes'),
(30, 'blog_charset', 'UTF-8', 'yes'),
(31, 'moderation_keys', '', 'no'),
(32, 'active_plugins', 'a:3:{i:0;s:36:"contact-form-7/wp-contact-form-7.php";i:1;s:43:"portfolio-post-type/portfolio-post-type.php";i:3;s:60:"wp-slick-slider-and-image-carousel/wp-slick-image-slider.php";}', 'yes'),
(33, 'category_base', '', 'yes'),
(34, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(35, 'comment_max_links', '2', 'yes'),
(36, 'gmt_offset', '0', 'yes'),
(37, 'default_email_category', '1', 'yes'),
(38, 'recently_edited', 'a:5:{i:0;s:92:"/var/www/html/wp_ofd/wp-content/plugins/wp-logo-showcase-responsive-slider-slider/readme.txt";i:1;s:99:"/var/www/html/wp_ofd/wp-content/plugins/wp-logo-showcase-responsive-slider-slider/logo-showcase.php";i:2;s:118:"/var/www/html/wp_ofd/wp-content/plugins/wp-logo-showcase-responsive-slider-slider/includes/logo-showcase-functions.php";i:4;s:76:"/var/www/html/wp_ofd/wp-content/plugins/contact-form-7/wp-contact-form-7.php";i:5;s:116:"/var/www/html/wp_ofd/wp-content/plugins/portfolio-post-type/includes/class-gamajo-single-entry-term-body-classes.php";}', 'no'),
(39, 'template', 'onefocusdigitals', 'yes'),
(40, 'stylesheet', 'onefocusdigitals', 'yes'),
(41, 'comment_whitelist', '1', 'yes'),
(42, 'blacklist_keys', '', 'no'),
(43, 'comment_registration', '0', 'yes'),
(44, 'html_type', 'text/html', 'yes'),
(45, 'use_trackback', '0', 'yes'),
(46, 'default_role', 'subscriber', 'yes'),
(47, 'db_version', '36686', 'yes'),
(48, 'uploads_use_yearmonth_folders', '1', 'yes'),
(49, 'upload_path', '', 'yes'),
(50, 'blog_public', '1', 'yes'),
(51, 'default_link_category', '2', 'yes'),
(52, 'show_on_front', 'posts', 'yes'),
(53, 'tag_base', '', 'yes'),
(54, 'show_avatars', '1', 'yes'),
(55, 'avatar_rating', 'G', 'yes'),
(56, 'upload_url_path', '', 'yes'),
(57, 'thumbnail_size_w', '150', 'yes'),
(58, 'thumbnail_size_h', '150', 'yes'),
(59, 'thumbnail_crop', '1', 'yes'),
(60, 'medium_size_w', '300', 'yes'),
(61, 'medium_size_h', '300', 'yes'),
(62, 'avatar_default', 'mystery', 'yes'),
(63, 'large_size_w', '1024', 'yes'),
(64, 'large_size_h', '1024', 'yes'),
(65, 'image_default_link_type', 'none', 'yes'),
(66, 'image_default_size', '', 'yes'),
(67, 'image_default_align', '', 'yes'),
(68, 'close_comments_for_old_posts', '0', 'yes'),
(69, 'close_comments_days_old', '14', 'yes'),
(70, 'thread_comments', '1', 'yes'),
(71, 'thread_comments_depth', '5', 'yes'),
(72, 'page_comments', '0', 'yes'),
(73, 'comments_per_page', '50', 'yes'),
(74, 'default_comments_page', 'newest', 'yes'),
(75, 'comment_order', 'asc', 'yes'),
(76, 'sticky_posts', 'a:0:{}', 'yes'),
(77, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(78, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'uninstall_plugins', 'a:1:{s:29:"portfolio-wp/portfolio-wp.php";s:18:"crp_uninstall_hook";}', 'no'),
(81, 'timezone_string', '', 'yes'),
(82, 'page_for_posts', '0', 'yes'),
(83, 'page_on_front', '0', 'yes'),
(84, 'default_post_format', '0', 'yes'),
(85, 'link_manager_enabled', '0', 'yes'),
(86, 'finished_splitting_shared_terms', '1', 'yes'),
(87, 'site_icon', '0', 'yes'),
(88, 'medium_large_size_w', '768', 'yes'),
(89, 'medium_large_size_h', '0', 'yes'),
(90, 'initial_db_version', '35700', 'yes'),
(91, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(92, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(93, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:18:"orphaned_widgets_1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(99, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'cron', 'a:4:{i:1462999349;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1462999357;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1463005880;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(106, 'nonce_key', 'n:dEjn@K<9lkKV:)6r.p-xSxK8JF>;EKf:_/UZUHj+12T;L<z`] K}B|ZqesCt3b', 'yes'),
(107, 'nonce_salt', 'N|1_d% ;?Jb}^oIs79*b.S://s]q}Nh@.l&0}^gj]8U`>;yBRr:eZ^d6#!J/ T,{', 'yes'),
(116, 'auth_key', ';.ERBB i*@CI[f;@;K7DBH)(Q$yCZq*dhVKl4i0S|0C=sf&]~d<C/IV%:x@>u{9c', 'yes'),
(117, 'auth_salt', '(WcctDe~X1@(XOr2WY~[jGS|!7I!4k/qXg@%%BV7mn3ues%5I{`[rKn^iZXjT.i)', 'yes'),
(118, 'logged_in_key', 'gHJE#k?wZOVH5SF9AD5U$7VS-G4`s;|x$ibe^o @;>A&(7d* ~V!owP$i);!i@ro', 'yes'),
(119, 'logged_in_salt', 'LR.[l3/clSxtR7YF_D +BWF-HTr[nGSx$Y?rr0!j10X/o}YtR:nSq-]y,+/hzvTn', 'yes'),
(129, '_transient_twentysixteen_categories', '1', 'yes'),
(138, '_transient_timeout_plugin_slugs', '1462756182', 'no'),
(139, '_transient_plugin_slugs', 'a:3:{i:0;s:36:"contact-form-7/wp-contact-form-7.php";i:1;s:43:"portfolio-post-type/portfolio-post-type.php";i:2;s:60:"wp-slick-slider-and-image-carousel/wp-slick-image-slider.php";}', 'no'),
(144, 'auto_core_update_notified', 'a:4:{s:4:"type";s:7:"success";s:5:"email";s:26:"irwing@onefocusdigital.com";s:7:"version";s:5:"4.5.2";s:9:"timestamp";i:1462627250;}', 'yes'),
(146, 'theme_mods_twentysixteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1459284542;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(147, 'current_theme', 'Theme One Focus Digital', 'yes'),
(148, 'theme_mods_onefocusdigitals', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}}', 'yes'),
(149, 'theme_switched', '', 'yes'),
(151, 'options-framework-theme', 'a:42:{s:7:"color_1";s:7:"#242328";s:7:"color_2";s:7:"#f44e26";s:7:"color_3";s:7:"#8c3927";s:4:"logo";s:62:"http://onefocusdigital.com/wp-content/uploads/2016/03/logo.jpg";s:7:"favicon";s:65:"http://onefocusdigital.com/wp-content/uploads/2016/03/favicon.png";s:6:"titulo";s:54:"One Focus Digital | Agencia de publicidad y Desarrollo";s:8:"telefono";s:17:"(+1) 786 355 4444";s:13:"telefono-link";s:12:"+17863554444";s:10:"email-link";s:24:"info@onefocusdigital.com";s:10:"bienvenido";s:10:"Bienvenido";s:11:"que_hacemos";s:14:"¿Que Hacemos?";s:15:"como_trabajamos";s:18:"¿Como trabajamos?";s:9:"proyectos";s:9:"Proyectos";s:11:"contactanos";s:11:"Contactanos";s:17:"titulo-bienvenido";s:37:"Agencia de Publicidad, Diseño & Web.";s:17:"imagen-bienvenido";s:64:"http://onefocusdigital.com/wp-content/uploads/2016/03/banner.jpg";s:20:"contenido-bienvenido";s:221:"One Focus Digital es un equipo estratégico, proveedor de servicios online y offline, realizamos consultoría personalizada, Software, ERP, CRM, E-commerce, Social Media, SEO, SEM entre otras oportunidades personalizadas.";s:18:"titulo-que-hacemos";s:40:"QUE HACEMOS, MÁS FACIL ¡IMPOSIBLE!----";s:16:"titulo-servicio1";s:8:"Branding";s:19:"contenido-servicio1";s:217:"Agencia de Publicidad, Diseño &amp; Web.Desarrollamos todos los atributos, tanto tangibles como intangibles, que crean la imagen de una empresa, producto y/o servicio. Cambiamos lo que es por lo que queremos que sea.";s:16:"titulo-servicio2";s:10:"Copy Write";s:19:"contenido-servicio2";s:191:"Nuestra especialidad es atraer la atención, aportar ideas creativas, concepciones estratégicas, arquitectura de información, diseños frescos y mensajes de un elevedo nivel de innovación.";s:16:"titulo-servicio3";s:3:"ROI";s:19:"contenido-servicio3";s:88:"Nos convertimos en tu aliado, construyendo juntos el éxito de tu idea o negocio actual.";s:22:"titulo-como-trabajamos";s:73:"LA INNOVACION SE PUEDE AMPLIFICAR A TRAVÉS DE ASOCIACIONES ESTRATÉGICAS";s:18:"imagen-estrategias";s:62:"http://onefocusdigital.com/wp-content/uploads/2016/03/idea.jpg";s:18:"titulo-estrategia1";s:12:"Planificamos";s:21:"contenido-estrategia1";s:93:"Una entrvista inicial, ya sea personal, por teléfono o vía Skype, totalmente personalizada.";s:18:"titulo-estrategia2";s:8:"Hablamos";s:21:"contenido-estrategia2";s:92:"De las estrategias de marketing particulares para cada cliente sean mayoristas o minoristas.";s:18:"titulo-estrategia3";s:8:"Pensamos";s:21:"contenido-estrategia3";s:90:"Que, concreatar una venta es importante pero lograr la fidelidad de los clientes en vital.";s:18:"titulo-estrategia4";s:7:"Creamos";s:21:"contenido-estrategia4";s:86:"Estrategias de valor que posicionan tu marca y mejoran la reantabilidad de tu negocio.";s:18:"titulo-estrategia5";s:11:"Comunicamos";s:21:"contenido-estrategia5";s:88:"De forma innovadora con tu público objetivo, generando un impacto positivo y diferente.";s:18:"titulo-estrategia6";s:8:"Crecemos";s:21:"contenido-estrategia6";s:95:"Como un equipo multidisciplinario enfocados en impulsar mejores negocios con nuestros clientes.";s:16:"titulo-proyectos";s:9:"PROYECTOS";s:18:"titulo-contactanos";s:30:"LA PRIMERA CONSULTA ES GRATIS!";s:17:"titulo-formulario";s:208:"Que vamos a lograr?, Como la vamos a hacer?, Que tiempo va a tardar?, Cuánto va a costar?, Cuáles son las forma de pago?, Cuáles son las metas y objetivos iniciales?, responderemos a todas tus preguntas...";s:22:"descripcion-formulario";s:0:"";}', 'yes'),
(235, 'WPLANG', 'es_ES', 'yes'),
(244, 'recently_activated', 'a:2:{s:59:"wp-logo-showcase-responsive-slider-slider/logo-showcase.php";i:1462669769;s:47:"accordion-slider-lite/accordion-slider-lite.php";i:1462647034;}', 'yes'),
(247, 'otw-portfolio-category_children', 'a:0:{}', 'yes'),
(252, 'widget_huge_it_portfolio_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(262, 'portfolio_category_children', 'a:0:{}', 'yes'),
(374, 'category_children', 'a:0:{}', 'yes'),
(426, 'rewrite_rules', 'a:146:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:12:"portfolio/?$";s:29:"index.php?post_type=portfolio";s:42:"portfolio/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?post_type=portfolio&feed=$matches[1]";s:37:"portfolio/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?post_type=portfolio&feed=$matches[1]";s:29:"portfolio/page/([0-9]{1,})/?$";s:47:"index.php?post_type=portfolio&paged=$matches[1]";s:15:"logoshowcase/?$";s:32:"index.php?post_type=logoshowcase";s:45:"logoshowcase/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?post_type=logoshowcase&feed=$matches[1]";s:40:"logoshowcase/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?post_type=logoshowcase&feed=$matches[1]";s:32:"logoshowcase/page/([0-9]{1,})/?$";s:50:"index.php?post_type=logoshowcase&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:37:"portfolio/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:47:"portfolio/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:67:"portfolio/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"portfolio/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"portfolio/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:43:"portfolio/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:26:"portfolio/([^/]+)/embed/?$";s:42:"index.php?portfolio=$matches[1]&embed=true";s:30:"portfolio/([^/]+)/trackback/?$";s:36:"index.php?portfolio=$matches[1]&tb=1";s:50:"portfolio/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?portfolio=$matches[1]&feed=$matches[2]";s:45:"portfolio/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?portfolio=$matches[1]&feed=$matches[2]";s:38:"portfolio/([^/]+)/page/?([0-9]{1,})/?$";s:49:"index.php?portfolio=$matches[1]&paged=$matches[2]";s:45:"portfolio/([^/]+)/comment-page-([0-9]{1,})/?$";s:49:"index.php?portfolio=$matches[1]&cpage=$matches[2]";s:34:"portfolio/([^/]+)(?:/([0-9]+))?/?$";s:48:"index.php?portfolio=$matches[1]&page=$matches[2]";s:26:"portfolio/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:36:"portfolio/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:56:"portfolio/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"portfolio/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"portfolio/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:32:"portfolio/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:59:"portfolio_category/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:57:"index.php?portfolio_category=$matches[1]&feed=$matches[2]";s:54:"portfolio_category/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:57:"index.php?portfolio_category=$matches[1]&feed=$matches[2]";s:35:"portfolio_category/([^/]+)/embed/?$";s:51:"index.php?portfolio_category=$matches[1]&embed=true";s:47:"portfolio_category/([^/]+)/page/?([0-9]{1,})/?$";s:58:"index.php?portfolio_category=$matches[1]&paged=$matches[2]";s:29:"portfolio_category/([^/]+)/?$";s:40:"index.php?portfolio_category=$matches[1]";s:54:"portfolio_tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?portfolio_tag=$matches[1]&feed=$matches[2]";s:49:"portfolio_tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?portfolio_tag=$matches[1]&feed=$matches[2]";s:30:"portfolio_tag/([^/]+)/embed/?$";s:46:"index.php?portfolio_tag=$matches[1]&embed=true";s:42:"portfolio_tag/([^/]+)/page/?([0-9]{1,})/?$";s:53:"index.php?portfolio_tag=$matches[1]&paged=$matches[2]";s:24:"portfolio_tag/([^/]+)/?$";s:35:"index.php?portfolio_tag=$matches[1]";s:40:"logoshowcase/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:50:"logoshowcase/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:70:"logoshowcase/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:65:"logoshowcase/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:65:"logoshowcase/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:46:"logoshowcase/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:29:"logoshowcase/([^/]+)/embed/?$";s:45:"index.php?logoshowcase=$matches[1]&embed=true";s:33:"logoshowcase/([^/]+)/trackback/?$";s:39:"index.php?logoshowcase=$matches[1]&tb=1";s:53:"logoshowcase/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:51:"index.php?logoshowcase=$matches[1]&feed=$matches[2]";s:48:"logoshowcase/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:51:"index.php?logoshowcase=$matches[1]&feed=$matches[2]";s:41:"logoshowcase/([^/]+)/page/?([0-9]{1,})/?$";s:52:"index.php?logoshowcase=$matches[1]&paged=$matches[2]";s:48:"logoshowcase/([^/]+)/comment-page-([0-9]{1,})/?$";s:52:"index.php?logoshowcase=$matches[1]&cpage=$matches[2]";s:37:"logoshowcase/([^/]+)(?:/([0-9]+))?/?$";s:51:"index.php?logoshowcase=$matches[1]&page=$matches[2]";s:29:"logoshowcase/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:39:"logoshowcase/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:59:"logoshowcase/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:54:"logoshowcase/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:54:"logoshowcase/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:35:"logoshowcase/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:64:"wplss_logo_showcase_cat/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:62:"index.php?wplss_logo_showcase_cat=$matches[1]&feed=$matches[2]";s:59:"wplss_logo_showcase_cat/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:62:"index.php?wplss_logo_showcase_cat=$matches[1]&feed=$matches[2]";s:40:"wplss_logo_showcase_cat/([^/]+)/embed/?$";s:56:"index.php?wplss_logo_showcase_cat=$matches[1]&embed=true";s:52:"wplss_logo_showcase_cat/([^/]+)/page/?([0-9]{1,})/?$";s:63:"index.php?wplss_logo_showcase_cat=$matches[1]&paged=$matches[2]";s:34:"wplss_logo_showcase_cat/([^/]+)/?$";s:45:"index.php?wplss_logo_showcase_cat=$matches[1]";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(439, 'nav_menu_options', 'a:1:{s:8:"auto_add";a:0:{}}', 'yes'),
(449, 'db_upgraded', '', 'yes'),
(452, 'can_compress_scripts', '0', 'yes'),
(461, 'wpcf7', 'a:2:{s:7:"version";s:5:"4.4.2";s:13:"bulk_validate";a:4:{s:9:"timestamp";i:1462384848;s:7:"version";s:5:"4.4.2";s:11:"count_valid";i:1;s:13:"count_invalid";i:0;}}', 'yes'),
(463, 'secret_key', 'zGQ#jGel2U]~l8s6j{qxLE_o_R%Kv9C(V/%TER,/0zSf8/D8(MVVAo]Oo#S&8%a}', 'yes'),
(478, '_site_transient_timeout_browser_cf52b6c25090d979cf2d649b8f42cbf8', '1463176845', 'yes'),
(479, '_site_transient_browser_cf52b6c25090d979cf2d649b8f42cbf8', 'a:9:{s:8:"platform";s:5:"Linux";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"47.0.2526.80";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(495, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:65:"https://downloads.wordpress.org/release/es_ES/wordpress-4.5.2.zip";s:6:"locale";s:5:"es_ES";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:65:"https://downloads.wordpress.org/release/es_ES/wordpress-4.5.2.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.5.2";s:7:"version";s:5:"4.5.2";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.4";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1462967608;s:15:"version_checked";s:5:"4.5.2";s:12:"translations";a:0:{}}', 'yes'),
(496, '_site_transient_timeout_browser_2c83f02ca1f2dfc5330472f94409809b', '1463238388', 'yes'),
(497, '_site_transient_browser_2c83f02ca1f2dfc5330472f94409809b', 'a:9:{s:8:"platform";s:5:"Linux";s:4:"name";s:7:"Firefox";s:7:"version";s:4:"38.0";s:10:"update_url";s:23:"http://www.firefox.com/";s:7:"img_src";s:50:"http://s.wordpress.org/images/browsers/firefox.png";s:11:"img_src_ssl";s:49:"https://wordpress.org/images/browsers/firefox.png";s:15:"current_version";s:2:"16";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(506, 'widget_bqw-accordion-slider-lite-widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(520, 'wplss_logo_showcase_cat_children', 'a:0:{}', 'yes'),
(523, '_site_transient_timeout_popular_importers_es_ES', '1462830236', 'yes'),
(524, '_site_transient_popular_importers_es_ES', 'a:2:{s:9:"importers";a:8:{s:7:"blogger";a:4:{s:4:"name";s:7:"Blogger";s:11:"description";s:86:"Install the Blogger importer to import posts, comments, and users from a Blogger blog.";s:11:"plugin-slug";s:16:"blogger-importer";s:11:"importer-id";s:7:"blogger";}s:9:"wpcat2tag";a:4:{s:4:"name";s:29:"Categories and Tags Converter";s:11:"description";s:109:"Install the category/tag converter to convert existing categories to tags or tags to categories, selectively.";s:11:"plugin-slug";s:18:"wpcat2tag-importer";s:11:"importer-id";s:9:"wpcat2tag";}s:11:"livejournal";a:4:{s:4:"name";s:11:"LiveJournal";s:11:"description";s:82:"Install the LiveJournal importer to import posts from LiveJournal using their API.";s:11:"plugin-slug";s:20:"livejournal-importer";s:11:"importer-id";s:11:"livejournal";}s:11:"movabletype";a:4:{s:4:"name";s:24:"Movable Type and TypePad";s:11:"description";s:99:"Install the Movable Type importer to import posts and comments from a Movable Type or TypePad blog.";s:11:"plugin-slug";s:20:"movabletype-importer";s:11:"importer-id";s:2:"mt";}s:4:"opml";a:4:{s:4:"name";s:8:"Blogroll";s:11:"description";s:61:"Install the blogroll importer to import links in OPML format.";s:11:"plugin-slug";s:13:"opml-importer";s:11:"importer-id";s:4:"opml";}s:3:"rss";a:4:{s:4:"name";s:3:"RSS";s:11:"description";s:58:"Install the RSS importer to import posts from an RSS feed.";s:11:"plugin-slug";s:12:"rss-importer";s:11:"importer-id";s:3:"rss";}s:6:"tumblr";a:4:{s:4:"name";s:6:"Tumblr";s:11:"description";s:84:"Install the Tumblr importer to import posts &amp; media from Tumblr using their API.";s:11:"plugin-slug";s:15:"tumblr-importer";s:11:"importer-id";s:6:"tumblr";}s:9:"wordpress";a:4:{s:4:"name";s:9:"WordPress";s:11:"description";s:130:"Install the WordPress importer to import posts, pages, comments, custom fields, categories, and tags from a WordPress export file.";s:11:"plugin-slug";s:18:"wordpress-importer";s:11:"importer-id";s:9:"wordpress";}}s:10:"translated";b:0;}', 'yes'),
(525, '_site_transient_timeout_available_translations', '1462668251', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(526, '_site_transient_available_translations', 'a:80:{s:2:"ar";a:8:{s:8:"language";s:2:"ar";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-10 15:55:55";s:12:"english_name";s:6:"Arabic";s:11:"native_name";s:14:"العربية";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/ar.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:2;s:3:"ara";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:3:"ary";a:8:{s:8:"language";s:3:"ary";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-13 14:44:00";s:12:"english_name";s:15:"Moroccan Arabic";s:11:"native_name";s:31:"العربية المغربية";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.5.2/ary.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:3;s:3:"ary";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:2:"az";a:8:{s:8:"language";s:2:"az";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-12 22:48:01";s:12:"english_name";s:11:"Azerbaijani";s:11:"native_name";s:16:"Azərbaycan dili";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/az.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:2;s:3:"aze";}s:7:"strings";a:1:{s:8:"continue";s:5:"Davam";}}s:3:"azb";a:8:{s:8:"language";s:3:"azb";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-12-11 22:42:10";s:12:"english_name";s:17:"South Azerbaijani";s:11:"native_name";s:29:"گؤنئی آذربایجان";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.2/azb.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:3;s:3:"azb";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"bg_BG";a:8:{s:8:"language";s:5:"bg_BG";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-03 14:05:41";s:12:"english_name";s:9:"Bulgarian";s:11:"native_name";s:18:"Български";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/bg_BG.zip";s:3:"iso";a:2:{i:1;s:2:"bg";i:2;s:3:"bul";}s:7:"strings";a:1:{s:8:"continue";s:12:"Напред";}}s:5:"bn_BD";a:8:{s:8:"language";s:5:"bn_BD";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2016-02-08 13:17:04";s:12:"english_name";s:7:"Bengali";s:11:"native_name";s:15:"বাংলা";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.4.2/bn_BD.zip";s:3:"iso";a:1:{i:1;s:2:"bn";}s:7:"strings";a:1:{s:8:"continue";s:23:"এগিয়ে চল.";}}s:5:"bs_BA";a:8:{s:8:"language";s:5:"bs_BA";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-19 23:16:37";s:12:"english_name";s:7:"Bosnian";s:11:"native_name";s:8:"Bosanski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/bs_BA.zip";s:3:"iso";a:2:{i:1;s:2:"bs";i:2;s:3:"bos";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:2:"ca";a:8:{s:8:"language";s:2:"ca";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-11 06:38:51";s:12:"english_name";s:7:"Catalan";s:11:"native_name";s:7:"Català";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/ca.zip";s:3:"iso";a:2:{i:1;s:2:"ca";i:2;s:3:"cat";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:3:"ceb";a:8:{s:8:"language";s:3:"ceb";s:7:"version";s:5:"4.4.3";s:7:"updated";s:19:"2016-02-16 15:34:57";s:12:"english_name";s:7:"Cebuano";s:11:"native_name";s:7:"Cebuano";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.3/ceb.zip";s:3:"iso";a:2:{i:2;s:3:"ceb";i:3;s:3:"ceb";}s:7:"strings";a:1:{s:8:"continue";s:7:"Padayun";}}s:5:"cs_CZ";a:8:{s:8:"language";s:5:"cs_CZ";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2016-02-11 18:32:36";s:12:"english_name";s:5:"Czech";s:11:"native_name";s:12:"Čeština‎";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.4.2/cs_CZ.zip";s:3:"iso";a:2:{i:1;s:2:"cs";i:2;s:3:"ces";}s:7:"strings";a:1:{s:8:"continue";s:11:"Pokračovat";}}s:2:"cy";a:8:{s:8:"language";s:2:"cy";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-11 14:21:06";s:12:"english_name";s:5:"Welsh";s:11:"native_name";s:7:"Cymraeg";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/cy.zip";s:3:"iso";a:2:{i:1;s:2:"cy";i:2;s:3:"cym";}s:7:"strings";a:1:{s:8:"continue";s:6:"Parhau";}}s:5:"da_DK";a:8:{s:8:"language";s:5:"da_DK";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-11 15:42:12";s:12:"english_name";s:6:"Danish";s:11:"native_name";s:5:"Dansk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/da_DK.zip";s:3:"iso";a:2:{i:1;s:2:"da";i:2;s:3:"dan";}s:7:"strings";a:1:{s:8:"continue";s:12:"Forts&#230;t";}}s:14:"de_CH_informal";a:8:{s:8:"language";s:14:"de_CH_informal";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-12 20:03:25";s:12:"english_name";s:23:"(Switzerland, Informal)";s:11:"native_name";s:21:"Deutsch (Schweiz, Du)";s:7:"package";s:73:"https://downloads.wordpress.org/translation/core/4.5.2/de_CH_informal.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:5:"de_DE";a:8:{s:8:"language";s:5:"de_DE";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-01 18:17:12";s:12:"english_name";s:6:"German";s:11:"native_name";s:7:"Deutsch";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/de_DE.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:12:"de_DE_formal";a:8:{s:8:"language";s:12:"de_DE_formal";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-01 18:17:51";s:12:"english_name";s:15:"German (Formal)";s:11:"native_name";s:13:"Deutsch (Sie)";s:7:"package";s:71:"https://downloads.wordpress.org/translation/core/4.5.2/de_DE_formal.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:5:"de_CH";a:8:{s:8:"language";s:5:"de_CH";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-12 19:26:41";s:12:"english_name";s:20:"German (Switzerland)";s:11:"native_name";s:17:"Deutsch (Schweiz)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/de_CH.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:2:"el";a:8:{s:8:"language";s:2:"el";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-13 21:14:17";s:12:"english_name";s:5:"Greek";s:11:"native_name";s:16:"Ελληνικά";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/el.zip";s:3:"iso";a:2:{i:1;s:2:"el";i:2;s:3:"ell";}s:7:"strings";a:1:{s:8:"continue";s:16:"Συνέχεια";}}s:5:"en_AU";a:8:{s:8:"language";s:5:"en_AU";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-13 06:26:11";s:12:"english_name";s:19:"English (Australia)";s:11:"native_name";s:19:"English (Australia)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/en_AU.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_GB";a:8:{s:8:"language";s:5:"en_GB";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-13 12:51:07";s:12:"english_name";s:12:"English (UK)";s:11:"native_name";s:12:"English (UK)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/en_GB.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_CA";a:8:{s:8:"language";s:5:"en_CA";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-10 05:23:57";s:12:"english_name";s:16:"English (Canada)";s:11:"native_name";s:16:"English (Canada)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/en_CA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_NZ";a:8:{s:8:"language";s:5:"en_NZ";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-26 02:00:05";s:12:"english_name";s:21:"English (New Zealand)";s:11:"native_name";s:21:"English (New Zealand)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/en_NZ.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_ZA";a:8:{s:8:"language";s:5:"en_ZA";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-28 11:29:02";s:12:"english_name";s:22:"English (South Africa)";s:11:"native_name";s:22:"English (South Africa)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/en_ZA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:2:"eo";a:8:{s:8:"language";s:2:"eo";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-11 10:58:49";s:12:"english_name";s:9:"Esperanto";s:11:"native_name";s:9:"Esperanto";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/eo.zip";s:3:"iso";a:2:{i:1;s:2:"eo";i:2;s:3:"epo";}s:7:"strings";a:1:{s:8:"continue";s:8:"Daŭrigi";}}s:5:"es_VE";a:8:{s:8:"language";s:5:"es_VE";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-28 13:08:25";s:12:"english_name";s:19:"Spanish (Venezuela)";s:11:"native_name";s:21:"Español de Venezuela";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/es_VE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_PE";a:8:{s:8:"language";s:5:"es_PE";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-16 17:35:43";s:12:"english_name";s:14:"Spanish (Peru)";s:11:"native_name";s:17:"Español de Perú";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/es_PE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_AR";a:8:{s:8:"language";s:5:"es_AR";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-19 21:32:12";s:12:"english_name";s:19:"Spanish (Argentina)";s:11:"native_name";s:21:"Español de Argentina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/es_AR.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_GT";a:8:{s:8:"language";s:5:"es_GT";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-13 12:43:00";s:12:"english_name";s:19:"Spanish (Guatemala)";s:11:"native_name";s:21:"Español de Guatemala";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/es_GT.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_MX";a:8:{s:8:"language";s:5:"es_MX";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-12 21:06:55";s:12:"english_name";s:16:"Spanish (Mexico)";s:11:"native_name";s:19:"Español de México";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/es_MX.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CO";a:8:{s:8:"language";s:5:"es_CO";s:7:"version";s:6:"4.3-RC";s:7:"updated";s:19:"2015-08-04 06:10:33";s:12:"english_name";s:18:"Spanish (Colombia)";s:11:"native_name";s:20:"Español de Colombia";s:7:"package";s:65:"https://downloads.wordpress.org/translation/core/4.3-RC/es_CO.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CL";a:8:{s:8:"language";s:5:"es_CL";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-13 01:09:28";s:12:"english_name";s:15:"Spanish (Chile)";s:11:"native_name";s:17:"Español de Chile";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/es_CL.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_ES";a:8:{s:8:"language";s:5:"es_ES";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-28 13:34:35";s:12:"english_name";s:15:"Spanish (Spain)";s:11:"native_name";s:8:"Español";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/es_ES.zip";s:3:"iso";a:1:{i:1;s:2:"es";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"et";a:8:{s:8:"language";s:2:"et";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-12 11:11:25";s:12:"english_name";s:8:"Estonian";s:11:"native_name";s:5:"Eesti";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/et.zip";s:3:"iso";a:2:{i:1;s:2:"et";i:2;s:3:"est";}s:7:"strings";a:1:{s:8:"continue";s:6:"Jätka";}}s:2:"eu";a:8:{s:8:"language";s:2:"eu";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-23 22:05:23";s:12:"english_name";s:6:"Basque";s:11:"native_name";s:7:"Euskara";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/eu.zip";s:3:"iso";a:2:{i:1;s:2:"eu";i:2;s:3:"eus";}s:7:"strings";a:1:{s:8:"continue";s:8:"Jarraitu";}}s:5:"fa_IR";a:8:{s:8:"language";s:5:"fa_IR";s:7:"version";s:5:"4.4.3";s:7:"updated";s:19:"2016-01-31 19:24:20";s:12:"english_name";s:7:"Persian";s:11:"native_name";s:10:"فارسی";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.4.3/fa_IR.zip";s:3:"iso";a:2:{i:1;s:2:"fa";i:2;s:3:"fas";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:2:"fi";a:8:{s:8:"language";s:2:"fi";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-10 18:44:50";s:12:"english_name";s:7:"Finnish";s:11:"native_name";s:5:"Suomi";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/fi.zip";s:3:"iso";a:2:{i:1;s:2:"fi";i:2;s:3:"fin";}s:7:"strings";a:1:{s:8:"continue";s:5:"Jatka";}}s:5:"fr_FR";a:8:{s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-29 13:55:46";s:12:"english_name";s:15:"French (France)";s:11:"native_name";s:9:"Français";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/fr_FR.zip";s:3:"iso";a:1:{i:1;s:2:"fr";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:5:"fr_BE";a:8:{s:8:"language";s:5:"fr_BE";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-11 07:33:47";s:12:"english_name";s:16:"French (Belgium)";s:11:"native_name";s:21:"Français de Belgique";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/fr_BE.zip";s:3:"iso";a:2:{i:1;s:2:"fr";i:2;s:3:"fra";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:5:"fr_CA";a:8:{s:8:"language";s:5:"fr_CA";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-29 19:30:46";s:12:"english_name";s:15:"French (Canada)";s:11:"native_name";s:19:"Français du Canada";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/fr_CA.zip";s:3:"iso";a:2:{i:1;s:2:"fr";i:2;s:3:"fra";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:2:"gd";a:8:{s:8:"language";s:2:"gd";s:7:"version";s:5:"4.3.4";s:7:"updated";s:19:"2015-09-24 15:25:30";s:12:"english_name";s:15:"Scottish Gaelic";s:11:"native_name";s:9:"Gàidhlig";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.3.4/gd.zip";s:3:"iso";a:3:{i:1;s:2:"gd";i:2;s:3:"gla";i:3;s:3:"gla";}s:7:"strings";a:1:{s:8:"continue";s:15:"Lean air adhart";}}s:5:"gl_ES";a:8:{s:8:"language";s:5:"gl_ES";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-22 23:06:30";s:12:"english_name";s:8:"Galician";s:11:"native_name";s:6:"Galego";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/gl_ES.zip";s:3:"iso";a:2:{i:1;s:2:"gl";i:2;s:3:"glg";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:3:"haz";a:8:{s:8:"language";s:3:"haz";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-12-05 00:59:09";s:12:"english_name";s:8:"Hazaragi";s:11:"native_name";s:15:"هزاره گی";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip";s:3:"iso";a:1:{i:3;s:3:"haz";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:5:"he_IL";a:8:{s:8:"language";s:5:"he_IL";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-16 13:14:11";s:12:"english_name";s:6:"Hebrew";s:11:"native_name";s:16:"עִבְרִית";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/he_IL.zip";s:3:"iso";a:1:{i:1;s:2:"he";}s:7:"strings";a:1:{s:8:"continue";s:8:"המשך";}}s:5:"hi_IN";a:8:{s:8:"language";s:5:"hi_IN";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-28 07:29:36";s:12:"english_name";s:5:"Hindi";s:11:"native_name";s:18:"हिन्दी";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/hi_IN.zip";s:3:"iso";a:2:{i:1;s:2:"hi";i:2;s:3:"hin";}s:7:"strings";a:1:{s:8:"continue";s:12:"जारी";}}s:2:"hr";a:8:{s:8:"language";s:2:"hr";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-07 12:13:44";s:12:"english_name";s:8:"Croatian";s:11:"native_name";s:8:"Hrvatski";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/hr.zip";s:3:"iso";a:2:{i:1;s:2:"hr";i:2;s:3:"hrv";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:5:"hu_HU";a:8:{s:8:"language";s:5:"hu_HU";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-03 06:34:38";s:12:"english_name";s:9:"Hungarian";s:11:"native_name";s:6:"Magyar";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/hu_HU.zip";s:3:"iso";a:2:{i:1;s:2:"hu";i:2;s:3:"hun";}s:7:"strings";a:1:{s:8:"continue";s:10:"Folytatás";}}s:2:"hy";a:8:{s:8:"language";s:2:"hy";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2016-02-04 07:13:54";s:12:"english_name";s:8:"Armenian";s:11:"native_name";s:14:"Հայերեն";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.4.2/hy.zip";s:3:"iso";a:2:{i:1;s:2:"hy";i:2;s:3:"hye";}s:7:"strings";a:1:{s:8:"continue";s:20:"Շարունակել";}}s:5:"id_ID";a:8:{s:8:"language";s:5:"id_ID";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-06 12:11:53";s:12:"english_name";s:10:"Indonesian";s:11:"native_name";s:16:"Bahasa Indonesia";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/id_ID.zip";s:3:"iso";a:2:{i:1;s:2:"id";i:2;s:3:"ind";}s:7:"strings";a:1:{s:8:"continue";s:9:"Lanjutkan";}}s:5:"is_IS";a:8:{s:8:"language";s:5:"is_IS";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-30 15:18:26";s:12:"english_name";s:9:"Icelandic";s:11:"native_name";s:9:"Íslenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/is_IS.zip";s:3:"iso";a:2:{i:1;s:2:"is";i:2;s:3:"isl";}s:7:"strings";a:1:{s:8:"continue";s:6:"Áfram";}}s:5:"it_IT";a:8:{s:8:"language";s:5:"it_IT";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-06 20:27:25";s:12:"english_name";s:7:"Italian";s:11:"native_name";s:8:"Italiano";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/it_IT.zip";s:3:"iso";a:2:{i:1;s:2:"it";i:2;s:3:"ita";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"ja";a:8:{s:8:"language";s:2:"ja";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-27 00:36:15";s:12:"english_name";s:8:"Japanese";s:11:"native_name";s:9:"日本語";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/ja.zip";s:3:"iso";a:1:{i:1;s:2:"ja";}s:7:"strings";a:1:{s:8:"continue";s:9:"続ける";}}s:5:"ka_GE";a:8:{s:8:"language";s:5:"ka_GE";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-11 09:29:35";s:12:"english_name";s:8:"Georgian";s:11:"native_name";s:21:"ქართული";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/ka_GE.zip";s:3:"iso";a:2:{i:1;s:2:"ka";i:2;s:3:"kat";}s:7:"strings";a:1:{s:8:"continue";s:30:"გაგრძელება";}}s:5:"ko_KR";a:8:{s:8:"language";s:5:"ko_KR";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-02 03:21:50";s:12:"english_name";s:6:"Korean";s:11:"native_name";s:9:"한국어";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/ko_KR.zip";s:3:"iso";a:2:{i:1;s:2:"ko";i:2;s:3:"kor";}s:7:"strings";a:1:{s:8:"continue";s:6:"계속";}}s:5:"lt_LT";a:8:{s:8:"language";s:5:"lt_LT";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-10 06:34:16";s:12:"english_name";s:10:"Lithuanian";s:11:"native_name";s:15:"Lietuvių kalba";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/lt_LT.zip";s:3:"iso";a:2:{i:1;s:2:"lt";i:2;s:3:"lit";}s:7:"strings";a:1:{s:8:"continue";s:6:"Tęsti";}}s:2:"mr";a:8:{s:8:"language";s:2:"mr";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-07 18:00:16";s:12:"english_name";s:7:"Marathi";s:11:"native_name";s:15:"मराठी";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/mr.zip";s:3:"iso";a:2:{i:1;s:2:"mr";i:2;s:3:"mar";}s:7:"strings";a:1:{s:8:"continue";s:25:"सुरु ठेवा";}}s:5:"ms_MY";a:8:{s:8:"language";s:5:"ms_MY";s:7:"version";s:5:"4.4.3";s:7:"updated";s:19:"2016-01-28 05:41:39";s:12:"english_name";s:5:"Malay";s:11:"native_name";s:13:"Bahasa Melayu";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.4.3/ms_MY.zip";s:3:"iso";a:2:{i:1;s:2:"ms";i:2;s:3:"msa";}s:7:"strings";a:1:{s:8:"continue";s:8:"Teruskan";}}s:5:"my_MM";a:8:{s:8:"language";s:5:"my_MM";s:7:"version";s:6:"4.1.11";s:7:"updated";s:19:"2015-03-26 15:57:42";s:12:"english_name";s:17:"Myanmar (Burmese)";s:11:"native_name";s:15:"ဗမာစာ";s:7:"package";s:65:"https://downloads.wordpress.org/translation/core/4.1.11/my_MM.zip";s:3:"iso";a:2:{i:1;s:2:"my";i:2;s:3:"mya";}s:7:"strings";a:1:{s:8:"continue";s:54:"ဆက်လက်လုပ်ဆောင်ပါ။";}}s:5:"nb_NO";a:8:{s:8:"language";s:5:"nb_NO";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-13 12:35:50";s:12:"english_name";s:19:"Norwegian (Bokmål)";s:11:"native_name";s:13:"Norsk bokmål";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/nb_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nb";i:2;s:3:"nob";}s:7:"strings";a:1:{s:8:"continue";s:8:"Fortsett";}}s:5:"nl_NL";a:8:{s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-02 15:16:51";s:12:"english_name";s:5:"Dutch";s:11:"native_name";s:10:"Nederlands";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/nl_NL.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:12:"nl_NL_formal";a:8:{s:8:"language";s:12:"nl_NL_formal";s:7:"version";s:5:"4.4.3";s:7:"updated";s:19:"2016-01-20 13:35:50";s:12:"english_name";s:14:"Dutch (Formal)";s:11:"native_name";s:20:"Nederlands (Formeel)";s:7:"package";s:71:"https://downloads.wordpress.org/translation/core/4.4.3/nl_NL_formal.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nn_NO";a:8:{s:8:"language";s:5:"nn_NO";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-11 07:36:04";s:12:"english_name";s:19:"Norwegian (Nynorsk)";s:11:"native_name";s:13:"Norsk nynorsk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/nn_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nn";i:2;s:3:"nno";}s:7:"strings";a:1:{s:8:"continue";s:9:"Hald fram";}}s:3:"oci";a:8:{s:8:"language";s:3:"oci";s:7:"version";s:5:"4.4.3";s:7:"updated";s:19:"2016-04-26 06:46:10";s:12:"english_name";s:7:"Occitan";s:11:"native_name";s:7:"Occitan";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.3/oci.zip";s:3:"iso";a:2:{i:1;s:2:"oc";i:2;s:3:"oci";}s:7:"strings";a:1:{s:8:"continue";s:9:"Contunhar";}}s:5:"pl_PL";a:8:{s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-06 19:38:10";s:12:"english_name";s:6:"Polish";s:11:"native_name";s:6:"Polski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/pl_PL.zip";s:3:"iso";a:2:{i:1;s:2:"pl";i:2;s:3:"pol";}s:7:"strings";a:1:{s:8:"continue";s:9:"Kontynuuj";}}s:2:"ps";a:8:{s:8:"language";s:2:"ps";s:7:"version";s:6:"4.1.11";s:7:"updated";s:19:"2015-03-29 22:19:48";s:12:"english_name";s:6:"Pashto";s:11:"native_name";s:8:"پښتو";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.1.11/ps.zip";s:3:"iso";a:2:{i:1;s:2:"ps";i:2;s:3:"pus";}s:7:"strings";a:1:{s:8:"continue";s:19:"دوام ورکړه";}}s:5:"pt_PT";a:8:{s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-27 10:21:39";s:12:"english_name";s:21:"Portuguese (Portugal)";s:11:"native_name";s:10:"Português";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/pt_PT.zip";s:3:"iso";a:1:{i:1;s:2:"pt";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"pt_BR";a:8:{s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-04 11:44:03";s:12:"english_name";s:19:"Portuguese (Brazil)";s:11:"native_name";s:20:"Português do Brasil";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/pt_BR.zip";s:3:"iso";a:2:{i:1;s:2:"pt";i:2;s:3:"por";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"ro_RO";a:8:{s:8:"language";s:5:"ro_RO";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-06 16:10:19";s:12:"english_name";s:8:"Romanian";s:11:"native_name";s:8:"Română";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/ro_RO.zip";s:3:"iso";a:2:{i:1;s:2:"ro";i:2;s:3:"ron";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuă";}}s:5:"ru_RU";a:8:{s:8:"language";s:5:"ru_RU";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-13 18:04:14";s:12:"english_name";s:7:"Russian";s:11:"native_name";s:14:"Русский";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/ru_RU.zip";s:3:"iso";a:2:{i:1;s:2:"ru";i:2;s:3:"rus";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продолжить";}}s:5:"sk_SK";a:8:{s:8:"language";s:5:"sk_SK";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-27 07:36:55";s:12:"english_name";s:6:"Slovak";s:11:"native_name";s:11:"Slovenčina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/sk_SK.zip";s:3:"iso";a:2:{i:1;s:2:"sk";i:2;s:3:"slk";}s:7:"strings";a:1:{s:8:"continue";s:12:"Pokračovať";}}s:5:"sl_SI";a:8:{s:8:"language";s:5:"sl_SI";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-11-26 00:00:18";s:12:"english_name";s:9:"Slovenian";s:11:"native_name";s:13:"Slovenščina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.4.2/sl_SI.zip";s:3:"iso";a:2:{i:1;s:2:"sl";i:2;s:3:"slv";}s:7:"strings";a:1:{s:8:"continue";s:8:"Nadaljuj";}}s:2:"sq";a:8:{s:8:"language";s:2:"sq";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-12 10:47:53";s:12:"english_name";s:8:"Albanian";s:11:"native_name";s:5:"Shqip";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/sq.zip";s:3:"iso";a:2:{i:1;s:2:"sq";i:2;s:3:"sqi";}s:7:"strings";a:1:{s:8:"continue";s:6:"Vazhdo";}}s:5:"sr_RS";a:8:{s:8:"language";s:5:"sr_RS";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-10 08:00:57";s:12:"english_name";s:7:"Serbian";s:11:"native_name";s:23:"Српски језик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/sr_RS.zip";s:3:"iso";a:2:{i:1;s:2:"sr";i:2;s:3:"srp";}s:7:"strings";a:1:{s:8:"continue";s:14:"Настави";}}s:5:"sv_SE";a:8:{s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-12 18:15:27";s:12:"english_name";s:7:"Swedish";s:11:"native_name";s:7:"Svenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/sv_SE.zip";s:3:"iso";a:2:{i:1;s:2:"sv";i:2;s:3:"swe";}s:7:"strings";a:1:{s:8:"continue";s:9:"Fortsätt";}}s:2:"th";a:8:{s:8:"language";s:2:"th";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-22 14:05:41";s:12:"english_name";s:4:"Thai";s:11:"native_name";s:9:"ไทย";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/th.zip";s:3:"iso";a:2:{i:1;s:2:"th";i:2;s:3:"tha";}s:7:"strings";a:1:{s:8:"continue";s:15:"ต่อไป";}}s:2:"tl";a:8:{s:8:"language";s:2:"tl";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-11-27 15:51:36";s:12:"english_name";s:7:"Tagalog";s:11:"native_name";s:7:"Tagalog";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.4.2/tl.zip";s:3:"iso";a:2:{i:1;s:2:"tl";i:2;s:3:"tgl";}s:7:"strings";a:1:{s:8:"continue";s:10:"Magpatuloy";}}s:5:"tr_TR";a:8:{s:8:"language";s:5:"tr_TR";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-21 01:31:12";s:12:"english_name";s:7:"Turkish";s:11:"native_name";s:8:"Türkçe";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/tr_TR.zip";s:3:"iso";a:2:{i:1;s:2:"tr";i:2;s:3:"tur";}s:7:"strings";a:1:{s:8:"continue";s:5:"Devam";}}s:5:"ug_CN";a:8:{s:8:"language";s:5:"ug_CN";s:7:"version";s:6:"4.1.11";s:7:"updated";s:19:"2015-03-26 16:45:38";s:12:"english_name";s:6:"Uighur";s:11:"native_name";s:9:"Uyƣurqə";s:7:"package";s:65:"https://downloads.wordpress.org/translation/core/4.1.11/ug_CN.zip";s:3:"iso";a:2:{i:1;s:2:"ug";i:2;s:3:"uig";}s:7:"strings";a:1:{s:8:"continue";s:26:"داۋاملاشتۇرۇش";}}s:2:"uk";a:8:{s:8:"language";s:2:"uk";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-05-03 13:19:01";s:12:"english_name";s:9:"Ukrainian";s:11:"native_name";s:20:"Українська";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.2/uk.zip";s:3:"iso";a:2:{i:1;s:2:"uk";i:2;s:3:"ukr";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продовжити";}}s:2:"vi";a:8:{s:8:"language";s:2:"vi";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-12-09 01:01:25";s:12:"english_name";s:10:"Vietnamese";s:11:"native_name";s:14:"Tiếng Việt";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.4.2/vi.zip";s:3:"iso";a:2:{i:1;s:2:"vi";i:2;s:3:"vie";}s:7:"strings";a:1:{s:8:"continue";s:12:"Tiếp tục";}}s:5:"zh_CN";a:8:{s:8:"language";s:5:"zh_CN";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-17 03:29:01";s:12:"english_name";s:15:"Chinese (China)";s:11:"native_name";s:12:"简体中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/zh_CN.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"继续";}}s:5:"zh_TW";a:8:{s:8:"language";s:5:"zh_TW";s:7:"version";s:5:"4.5.2";s:7:"updated";s:19:"2016-04-12 09:08:07";s:12:"english_name";s:16:"Chinese (Taiwan)";s:11:"native_name";s:12:"繁體中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.2/zh_TW.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}}', 'yes'),
(531, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1462674747', 'yes'),
(532, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'a:100:{s:6:"widget";a:3:{s:4:"name";s:6:"widget";s:4:"slug";s:6:"widget";s:5:"count";s:4:"5862";}s:4:"post";a:3:{s:4:"name";s:4:"Post";s:4:"slug";s:4:"post";s:5:"count";s:4:"3630";}s:6:"plugin";a:3:{s:4:"name";s:6:"plugin";s:4:"slug";s:6:"plugin";s:5:"count";s:4:"3598";}s:5:"admin";a:3:{s:4:"name";s:5:"admin";s:4:"slug";s:5:"admin";s:5:"count";s:4:"3109";}s:5:"posts";a:3:{s:4:"name";s:5:"posts";s:4:"slug";s:5:"posts";s:5:"count";s:4:"2782";}s:9:"shortcode";a:3:{s:4:"name";s:9:"shortcode";s:4:"slug";s:9:"shortcode";s:5:"count";s:4:"2346";}s:7:"sidebar";a:3:{s:4:"name";s:7:"sidebar";s:4:"slug";s:7:"sidebar";s:5:"count";s:4:"2205";}s:6:"google";a:3:{s:4:"name";s:6:"google";s:4:"slug";s:6:"google";s:5:"count";s:4:"2088";}s:7:"twitter";a:3:{s:4:"name";s:7:"twitter";s:4:"slug";s:7:"twitter";s:5:"count";s:4:"2032";}s:4:"page";a:3:{s:4:"name";s:4:"page";s:4:"slug";s:4:"page";s:5:"count";s:4:"2007";}s:6:"images";a:3:{s:4:"name";s:6:"images";s:4:"slug";s:6:"images";s:5:"count";s:4:"1982";}s:8:"comments";a:3:{s:4:"name";s:8:"comments";s:4:"slug";s:8:"comments";s:5:"count";s:4:"1929";}s:5:"image";a:3:{s:4:"name";s:5:"image";s:4:"slug";s:5:"image";s:5:"count";s:4:"1863";}s:8:"facebook";a:3:{s:4:"name";s:8:"Facebook";s:4:"slug";s:8:"facebook";s:5:"count";s:4:"1676";}s:11:"woocommerce";a:3:{s:4:"name";s:11:"woocommerce";s:4:"slug";s:11:"woocommerce";s:5:"count";s:4:"1641";}s:3:"seo";a:3:{s:4:"name";s:3:"seo";s:4:"slug";s:3:"seo";s:5:"count";s:4:"1568";}s:9:"wordpress";a:3:{s:4:"name";s:9:"wordpress";s:4:"slug";s:9:"wordpress";s:5:"count";s:4:"1530";}s:6:"social";a:3:{s:4:"name";s:6:"social";s:4:"slug";s:6:"social";s:5:"count";s:4:"1371";}s:7:"gallery";a:3:{s:4:"name";s:7:"gallery";s:4:"slug";s:7:"gallery";s:5:"count";s:4:"1304";}s:5:"links";a:3:{s:4:"name";s:5:"links";s:4:"slug";s:5:"links";s:5:"count";s:4:"1285";}s:5:"email";a:3:{s:4:"name";s:5:"email";s:4:"slug";s:5:"email";s:5:"count";s:4:"1218";}s:7:"widgets";a:3:{s:4:"name";s:7:"widgets";s:4:"slug";s:7:"widgets";s:5:"count";s:4:"1103";}s:5:"pages";a:3:{s:4:"name";s:5:"pages";s:4:"slug";s:5:"pages";s:5:"count";s:4:"1082";}s:6:"jquery";a:3:{s:4:"name";s:6:"jquery";s:4:"slug";s:6:"jquery";s:5:"count";s:4:"1005";}s:5:"media";a:3:{s:4:"name";s:5:"media";s:4:"slug";s:5:"media";s:5:"count";s:3:"981";}s:9:"ecommerce";a:3:{s:4:"name";s:9:"ecommerce";s:4:"slug";s:9:"ecommerce";s:5:"count";s:3:"970";}s:3:"rss";a:3:{s:4:"name";s:3:"rss";s:4:"slug";s:3:"rss";s:5:"count";s:3:"912";}s:5:"video";a:3:{s:4:"name";s:5:"video";s:4:"slug";s:5:"video";s:5:"count";s:3:"912";}s:4:"ajax";a:3:{s:4:"name";s:4:"AJAX";s:4:"slug";s:4:"ajax";s:5:"count";s:3:"908";}s:7:"content";a:3:{s:4:"name";s:7:"content";s:4:"slug";s:7:"content";s:5:"count";s:3:"899";}s:5:"login";a:3:{s:4:"name";s:5:"login";s:4:"slug";s:5:"login";s:5:"count";s:3:"898";}s:10:"javascript";a:3:{s:4:"name";s:10:"javascript";s:4:"slug";s:10:"javascript";s:5:"count";s:3:"832";}s:10:"responsive";a:3:{s:4:"name";s:10:"responsive";s:4:"slug";s:10:"responsive";s:5:"count";s:3:"820";}s:10:"buddypress";a:3:{s:4:"name";s:10:"buddypress";s:4:"slug";s:10:"buddypress";s:5:"count";s:3:"790";}s:8:"security";a:3:{s:4:"name";s:8:"security";s:4:"slug";s:8:"security";s:5:"count";s:3:"778";}s:10:"e-commerce";a:3:{s:4:"name";s:10:"e-commerce";s:4:"slug";s:10:"e-commerce";s:5:"count";s:3:"763";}s:5:"photo";a:3:{s:4:"name";s:5:"photo";s:4:"slug";s:5:"photo";s:5:"count";s:3:"756";}s:7:"youtube";a:3:{s:4:"name";s:7:"youtube";s:4:"slug";s:7:"youtube";s:5:"count";s:3:"752";}s:4:"spam";a:3:{s:4:"name";s:4:"spam";s:4:"slug";s:4:"spam";s:5:"count";s:3:"746";}s:5:"share";a:3:{s:4:"name";s:5:"Share";s:4:"slug";s:5:"share";s:5:"count";s:3:"745";}s:4:"feed";a:3:{s:4:"name";s:4:"feed";s:4:"slug";s:4:"feed";s:5:"count";s:3:"742";}s:4:"link";a:3:{s:4:"name";s:4:"link";s:4:"slug";s:4:"link";s:5:"count";s:3:"741";}s:8:"category";a:3:{s:4:"name";s:8:"category";s:4:"slug";s:8:"category";s:5:"count";s:3:"703";}s:9:"analytics";a:3:{s:4:"name";s:9:"analytics";s:4:"slug";s:9:"analytics";s:5:"count";s:3:"694";}s:6:"photos";a:3:{s:4:"name";s:6:"photos";s:4:"slug";s:6:"photos";s:5:"count";s:3:"689";}s:3:"css";a:3:{s:4:"name";s:3:"CSS";s:4:"slug";s:3:"css";s:5:"count";s:3:"683";}s:5:"embed";a:3:{s:4:"name";s:5:"embed";s:4:"slug";s:5:"embed";s:5:"count";s:3:"683";}s:4:"form";a:3:{s:4:"name";s:4:"form";s:4:"slug";s:4:"form";s:5:"count";s:3:"680";}s:6:"search";a:3:{s:4:"name";s:6:"search";s:4:"slug";s:6:"search";s:5:"count";s:3:"661";}s:6:"slider";a:3:{s:4:"name";s:6:"slider";s:4:"slug";s:6:"slider";s:5:"count";s:3:"652";}s:6:"custom";a:3:{s:4:"name";s:6:"custom";s:4:"slug";s:6:"custom";s:5:"count";s:3:"646";}s:9:"slideshow";a:3:{s:4:"name";s:9:"slideshow";s:4:"slug";s:9:"slideshow";s:5:"count";s:3:"642";}s:5:"stats";a:3:{s:4:"name";s:5:"stats";s:4:"slug";s:5:"stats";s:5:"count";s:3:"614";}s:6:"button";a:3:{s:4:"name";s:6:"button";s:4:"slug";s:6:"button";s:5:"count";s:3:"611";}s:4:"menu";a:3:{s:4:"name";s:4:"menu";s:4:"slug";s:4:"menu";s:5:"count";s:3:"599";}s:7:"comment";a:3:{s:4:"name";s:7:"comment";s:4:"slug";s:7:"comment";s:5:"count";s:3:"597";}s:9:"dashboard";a:3:{s:4:"name";s:9:"dashboard";s:4:"slug";s:9:"dashboard";s:5:"count";s:3:"595";}s:5:"theme";a:3:{s:4:"name";s:5:"theme";s:4:"slug";s:5:"theme";s:5:"count";s:3:"593";}s:4:"tags";a:3:{s:4:"name";s:4:"tags";s:4:"slug";s:4:"tags";s:5:"count";s:3:"588";}s:10:"categories";a:3:{s:4:"name";s:10:"categories";s:4:"slug";s:10:"categories";s:5:"count";s:3:"579";}s:6:"mobile";a:3:{s:4:"name";s:6:"mobile";s:4:"slug";s:6:"mobile";s:5:"count";s:3:"569";}s:10:"statistics";a:3:{s:4:"name";s:10:"statistics";s:4:"slug";s:10:"statistics";s:5:"count";s:3:"567";}s:3:"ads";a:3:{s:4:"name";s:3:"ads";s:4:"slug";s:3:"ads";s:5:"count";s:3:"562";}s:6:"editor";a:3:{s:4:"name";s:6:"editor";s:4:"slug";s:6:"editor";s:5:"count";s:3:"551";}s:4:"user";a:3:{s:4:"name";s:4:"user";s:4:"slug";s:4:"user";s:5:"count";s:3:"551";}s:5:"users";a:3:{s:4:"name";s:5:"users";s:4:"slug";s:5:"users";s:5:"count";s:3:"536";}s:4:"list";a:3:{s:4:"name";s:4:"list";s:4:"slug";s:4:"list";s:5:"count";s:3:"534";}s:7:"plugins";a:3:{s:4:"name";s:7:"plugins";s:4:"slug";s:7:"plugins";s:5:"count";s:3:"517";}s:9:"affiliate";a:3:{s:4:"name";s:9:"affiliate";s:4:"slug";s:9:"affiliate";s:5:"count";s:3:"516";}s:7:"picture";a:3:{s:4:"name";s:7:"picture";s:4:"slug";s:7:"picture";s:5:"count";s:3:"515";}s:6:"simple";a:3:{s:4:"name";s:6:"simple";s:4:"slug";s:6:"simple";s:5:"count";s:3:"506";}s:9:"multisite";a:3:{s:4:"name";s:9:"multisite";s:4:"slug";s:9:"multisite";s:5:"count";s:3:"506";}s:12:"social-media";a:3:{s:4:"name";s:12:"social media";s:4:"slug";s:12:"social-media";s:5:"count";s:3:"503";}s:12:"contact-form";a:3:{s:4:"name";s:12:"contact form";s:4:"slug";s:12:"contact-form";s:5:"count";s:3:"501";}s:7:"contact";a:3:{s:4:"name";s:7:"contact";s:4:"slug";s:7:"contact";s:5:"count";s:3:"482";}s:8:"pictures";a:3:{s:4:"name";s:8:"pictures";s:4:"slug";s:8:"pictures";s:5:"count";s:3:"464";}s:4:"shop";a:3:{s:4:"name";s:4:"shop";s:4:"slug";s:4:"shop";s:5:"count";s:3:"456";}s:9:"marketing";a:3:{s:4:"name";s:9:"marketing";s:4:"slug";s:9:"marketing";s:5:"count";s:3:"454";}s:3:"api";a:3:{s:4:"name";s:3:"api";s:4:"slug";s:3:"api";s:5:"count";s:3:"449";}s:3:"url";a:3:{s:4:"name";s:3:"url";s:4:"slug";s:3:"url";s:5:"count";s:3:"448";}s:10:"navigation";a:3:{s:4:"name";s:10:"navigation";s:4:"slug";s:10:"navigation";s:5:"count";s:3:"440";}s:4:"html";a:3:{s:4:"name";s:4:"html";s:4:"slug";s:4:"html";s:5:"count";s:3:"439";}s:10:"newsletter";a:3:{s:4:"name";s:10:"newsletter";s:4:"slug";s:10:"newsletter";s:5:"count";s:3:"425";}s:4:"meta";a:3:{s:4:"name";s:4:"meta";s:4:"slug";s:4:"meta";s:5:"count";s:3:"422";}s:5:"flash";a:3:{s:4:"name";s:5:"flash";s:4:"slug";s:5:"flash";s:5:"count";s:3:"422";}s:8:"tracking";a:3:{s:4:"name";s:8:"tracking";s:4:"slug";s:8:"tracking";s:5:"count";s:3:"421";}s:6:"events";a:3:{s:4:"name";s:6:"events";s:4:"slug";s:6:"events";s:5:"count";s:3:"418";}s:8:"calendar";a:3:{s:4:"name";s:8:"calendar";s:4:"slug";s:8:"calendar";s:5:"count";s:3:"417";}s:4:"news";a:3:{s:4:"name";s:4:"News";s:4:"slug";s:4:"news";s:5:"count";s:3:"408";}s:3:"tag";a:3:{s:4:"name";s:3:"tag";s:4:"slug";s:3:"tag";s:5:"count";s:3:"408";}s:10:"shortcodes";a:3:{s:4:"name";s:10:"shortcodes";s:4:"slug";s:10:"shortcodes";s:5:"count";s:3:"404";}s:11:"advertising";a:3:{s:4:"name";s:11:"advertising";s:4:"slug";s:11:"advertising";s:5:"count";s:3:"403";}s:9:"thumbnail";a:3:{s:4:"name";s:9:"thumbnail";s:4:"slug";s:9:"thumbnail";s:5:"count";s:3:"402";}s:6:"paypal";a:3:{s:4:"name";s:6:"paypal";s:4:"slug";s:6:"paypal";s:5:"count";s:3:"398";}s:6:"upload";a:3:{s:4:"name";s:6:"upload";s:4:"slug";s:6:"upload";s:5:"count";s:3:"397";}s:12:"notification";a:3:{s:4:"name";s:12:"notification";s:4:"slug";s:12:"notification";s:5:"count";s:3:"396";}s:7:"sharing";a:3:{s:4:"name";s:7:"sharing";s:4:"slug";s:7:"sharing";s:5:"count";s:3:"392";}s:4:"text";a:3:{s:4:"name";s:4:"text";s:4:"slug";s:4:"text";s:5:"count";s:3:"391";}s:8:"linkedin";a:3:{s:4:"name";s:8:"linkedin";s:4:"slug";s:8:"linkedin";s:5:"count";s:3:"389";}s:4:"code";a:3:{s:4:"name";s:4:"code";s:4:"slug";s:4:"code";s:5:"count";s:3:"389";}}', 'yes'),
(536, '_site_transient_update_plugins', 'O:8:"stdClass":4:{s:12:"last_checked";i:1462967608;s:8:"response";a:0:{}s:12:"translations";a:0:{}s:9:"no_update";a:3:{s:36:"contact-form-7/wp-contact-form-7.php";O:8:"stdClass":6:{s:2:"id";s:3:"790";s:4:"slug";s:14:"contact-form-7";s:6:"plugin";s:36:"contact-form-7/wp-contact-form-7.php";s:11:"new_version";s:5:"4.4.2";s:3:"url";s:45:"https://wordpress.org/plugins/contact-form-7/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/contact-form-7.4.4.2.zip";}s:43:"portfolio-post-type/portfolio-post-type.php";O:8:"stdClass":6:{s:2:"id";s:5:"25063";s:4:"slug";s:19:"portfolio-post-type";s:6:"plugin";s:43:"portfolio-post-type/portfolio-post-type.php";s:11:"new_version";s:5:"0.9.2";s:3:"url";s:50:"https://wordpress.org/plugins/portfolio-post-type/";s:7:"package";s:68:"https://downloads.wordpress.org/plugin/portfolio-post-type.0.9.2.zip";}s:60:"wp-slick-slider-and-image-carousel/wp-slick-image-slider.php";O:8:"stdClass":7:{s:2:"id";s:5:"66437";s:4:"slug";s:34:"wp-slick-slider-and-image-carousel";s:6:"plugin";s:60:"wp-slick-slider-and-image-carousel/wp-slick-image-slider.php";s:11:"new_version";s:5:"1.2.3";s:3:"url";s:65:"https://wordpress.org/plugins/wp-slick-slider-and-image-carousel/";s:7:"package";s:77:"https://downloads.wordpress.org/plugin/wp-slick-slider-and-image-carousel.zip";s:14:"upgrade_notice";s:22:"Fixed some css issues.";}}}', 'yes'),
(537, 'wpsisac_slider-category_children', 'a:0:{}', 'yes'),
(557, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1462967609;s:7:"checked";a:2:{s:16:"onefocusdigitals";s:0:"";s:13:"twentysixteen";s:3:"1.0";}s:8:"response";a:1:{s:13:"twentysixteen";a:4:{s:5:"theme";s:13:"twentysixteen";s:11:"new_version";s:3:"1.2";s:3:"url";s:43:"https://wordpress.org/themes/twentysixteen/";s:7:"package";s:59:"https://downloads.wordpress.org/theme/twentysixteen.1.2.zip";}}s:12:"translations";a:0:{}}', 'yes'),
(565, '_site_transient_timeout_theme_roots', '1462969408', 'yes'),
(566, '_site_transient_theme_roots', 'a:2:{s:16:"onefocusdigitals";s:7:"/themes";s:13:"twentysixteen";s:7:"/themes";}', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE IF NOT EXISTS `wp_postmeta` (
`meta_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(4, 5, '_wp_attached_file', '2016/03/logo.jpg'),
(5, 5, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:364;s:6:"height";i:48;s:4:"file";s:16:"2016/03/logo.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"logo-150x48.jpg";s:5:"width";i:150;s:6:"height";i:48;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"logo-300x40.jpg";s:5:"width";i:300;s:6:"height";i:40;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(6, 6, '_wp_attached_file', '2016/03/favicon.png'),
(7, 6, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:32;s:6:"height";i:32;s:4:"file";s:19:"2016/03/favicon.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(8, 7, '_wp_attached_file', '2016/03/banner.jpg'),
(9, 7, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:232;s:6:"height";i:232;s:4:"file";s:18:"2016/03/banner.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"banner-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(10, 8, '_wp_attached_file', '2016/03/idea.jpg'),
(11, 8, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:389;s:6:"height";i:402;s:4:"file";s:16:"2016/03/idea.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"idea-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"idea-290x300.jpg";s:5:"width";i:290;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(12, 9, '_edit_last', '1'),
(13, 9, '_edit_lock', '1459377137:1'),
(14, 10, '_edit_last', '1'),
(15, 10, '_edit_lock', '1462643261:1'),
(16, 11, '_wp_attached_file', '2016/03/work1.jpg'),
(17, 11, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:280;s:4:"file";s:17:"2016/03/work1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"work1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"work1-300x280.jpg";s:5:"width";i:300;s:6:"height";i:280;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(18, 10, '_thumbnail_id', '11'),
(22, 15, '_wp_attached_file', '2016/03/work2.jpg'),
(23, 15, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:303;s:6:"height";i:283;s:4:"file";s:17:"2016/03/work2.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"work2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"work2-300x280.jpg";s:5:"width";i:300;s:6:"height";i:280;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(24, 14, '_thumbnail_id', '15'),
(25, 14, '_edit_last', '1'),
(26, 14, '_edit_lock', '1462891717:1'),
(27, 17, '_edit_last', '1'),
(28, 17, '_edit_lock', '1462890164:1'),
(29, 18, '_wp_attached_file', '2016/03/work3.jpg'),
(30, 18, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:302;s:6:"height";i:282;s:4:"file";s:17:"2016/03/work3.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"work3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"work3-300x280.jpg";s:5:"width";i:300;s:6:"height";i:280;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(31, 17, '_thumbnail_id', '18'),
(35, 32, '_edit_last', '1'),
(36, 32, '_edit_lock', '1461886394:1'),
(37, 32, '_wp_trash_meta_status', 'publish'),
(38, 32, '_wp_trash_meta_time', '1461886552'),
(52, 64, '_edit_last', '1'),
(53, 64, '_edit_lock', '1462251406:1'),
(54, 64, '_wp_trash_meta_status', 'draft'),
(55, 64, '_wp_trash_meta_time', '1462251685'),
(67, 75, '_edit_last', '1'),
(68, 75, '_wp_page_template', 'page.php'),
(69, 75, '_edit_lock', '1462380985:1'),
(71, 80, '_edit_last', '1'),
(72, 80, '_edit_lock', '1462646877:1'),
(73, 80, '_wp_page_template', 'page.php'),
(75, 75, '_thumbnail_id', '8'),
(76, 100, '_form', '<div class="form-group col-md-6">\n	<label>Nombre</label>\n        [text* name class:form-control  placeholder "Nombre"]\n</div>\n\n<div class="form-group col-md-6">\n	<label>Apellido</label>\n        [text* last_name class:form-control  placeholder "Apellido"]\n</div>\n\n<div class="form-group col-md-6">\n	<label>Teléfono</label>\n        [tel* phone_contact class:form-control  placeholder "Telefono"]\n</div>\n\n<div class="form-group col-md-6">\n	<label>Email</label>\n        [email* email class:form-control  placeholder "Email"]\n</div>\n\n<div class="form-group col-md-12">\n	<label>¿Como escucho sobre nosotros?</label>\n        [select* reference class:form-control "" "Recomendado por alguien" "Televisión" "Radio" "Periodico" "Google"]\n</div>\n\n<div class="form-group col-md-12">\n	<label>Describenos tu idea</label>\n        [textarea* describe_your_idea 40x2  class:form-control placeholder "Tu idea nose importa..."]\n</div>\n<div class="form-group col-md-12 quiz-style">\n\n <div class="input-group">\n  <div class="input-group-addon">3 + 1</div>\n  [quiz quiz-618   class:form-control "|4"]\n </div>\n</div>\n\n<div class="col-md-12 form-group ">\n        [submit class:boton-enviar class:form-control "Enviar"]\n</div>'),
(77, 100, '_mail', 'a:8:{s:7:"subject";s:32:"OneFocusDigital "[your-subject]"";s:6:"sender";s:40:"[your-name] <irwing@onefocusdigital.com>";s:4:"body";s:200:"De: [your-name] <[your-email]>\nAsunto: [your-subject]\n\nCuerpo del mensaje:\n[your-message]\n\n--\nEste mensaje se ha enviado desde un formulario de contacto en OneFocusDigital (http://onefocusdigital.com)";s:9:"recipient";s:26:"irwing@onefocusdigital.com";s:18:"additional_headers";s:17:"Reply-To: [email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(78, 100, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:32:"OneFocusDigital "[your-subject]"";s:6:"sender";s:44:"OneFocusDigital <irwing@onefocusdigital.com>";s:4:"body";s:145:"Cuerpo del mensaje:\n[your-message]\n\n--\nEste mensaje se ha enviado desde un formulario de contacto en OneFocusDigital (http://onefocusdigital.com)";s:9:"recipient";s:12:"[your-email]";s:18:"additional_headers";s:36:"Reply-To: irwing@onefocusdigital.com";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(79, 100, '_messages', 'a:23:{s:12:"mail_sent_ok";s:40:"Gracias por tu mensaje. Ha sido enviado.";s:12:"mail_sent_ng";s:85:"Hubo un error intentando enviar tu mensaje. Por favor inténtalo de nuevo más tarde.";s:16:"validation_error";s:74:"Uno o más campos tienen un error. Por favor revisa e inténtalo de nuevo.";s:4:"spam";s:85:"Hubo un error intentando enviar tu mensaje. Por favor inténtalo de nuevo más tarde.";s:12:"accept_terms";s:69:"Debes aceptar los términos y condiciones antes de enviar tu mensaje.";s:16:"invalid_required";s:24:"El campo es obligatorio.";s:16:"invalid_too_long";s:28:"El campo es demasiado largo.";s:17:"invalid_too_short";s:28:"El campo es demasiado corto.";s:12:"invalid_date";s:34:"El formato de fecha es incorrecto.";s:14:"date_too_early";s:50:"La fecha es anterior a la más temprana permitida.";s:13:"date_too_late";s:50:"La fecha es posterior a la más tardía permitida.";s:13:"upload_failed";s:46:"Hubo un error desconocido subiendo el archivo.";s:24:"upload_file_type_invalid";s:52:"No tienes permisos para subir archivos de este tipo.";s:21:"upload_file_too_large";s:31:"El archivo es demasiado grande.";s:23:"upload_failed_php_error";s:43:"Se ha producido un error subiendo la imagen";s:14:"invalid_number";s:36:"El formato de número no es válido.";s:16:"number_too_small";s:45:"El número es menor que el mínimo permitido.";s:16:"number_too_large";s:45:"El número es mayor que el máximo permitido.";s:23:"quiz_answer_not_correct";s:44:"La respuesta al cuestionario no es correcta.";s:17:"captcha_not_match";s:37:"El código introducido es incorrecto.";s:13:"invalid_email";s:71:"La dirección de correo electrónico que has introducido no es válida.";s:11:"invalid_url";s:21:"La URL no es válida.";s:11:"invalid_tel";s:38:"El número de teléfono no es válido.";}'),
(80, 100, '_additional_settings', ''),
(81, 100, '_locale', 'es_ES'),
(303, 160, '_thumbnail_id', '7'),
(304, 160, '_edit_last', '1'),
(305, 160, '_edit_lock', '1462894117:1'),
(306, 160, '_wp_old_slug', 'lol'),
(307, 163, '_edit_last', '1'),
(308, 163, '_edit_lock', '1462894286:1'),
(309, 165, '_thumbnail_id', '7'),
(310, 165, '_edit_last', '1'),
(311, 165, '_edit_lock', '1462895121:1'),
(312, 167, '_edit_last', '1'),
(313, 167, '_edit_lock', '1462895138:1'),
(314, 167, '_thumbnail_id', '7'),
(315, 169, '_edit_last', '1'),
(316, 169, '_edit_lock', '1462895157:1'),
(317, 169, '_thumbnail_id', '7'),
(318, 171, '_edit_last', '1'),
(319, 171, '_edit_lock', '1462895180:1'),
(320, 171, '_thumbnail_id', '7'),
(321, 173, '_edit_last', '1'),
(322, 173, '_edit_lock', '1462895216:1'),
(323, 173, '_thumbnail_id', '7'),
(324, 175, '_edit_last', '1'),
(325, 175, '_edit_lock', '1462896787:1'),
(326, 175, '_thumbnail_id', '8'),
(333, 173, '_wp_old_slug', 'prueba-6__trashed'),
(334, 175, '_wp_old_slug', 'prueba-7__trashed'),
(338, 177, '_edit_last', '1'),
(339, 177, '_edit_lock', '1462898003:1'),
(340, 177, '_thumbnail_id', '7'),
(341, 179, '_edit_last', '1'),
(342, 179, '_edit_lock', '1462969895:1'),
(343, 179, '_thumbnail_id', '7');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE IF NOT EXISTS `wp_posts` (
`ID` bigint(20) unsigned NOT NULL,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(5, 1, '2016-03-30 16:45:40', '2016-03-30 16:45:40', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2016-03-30 16:45:40', '2016-03-30 16:45:40', '', 0, 'http://onefocusdigital.com/wp-content/uploads/2016/03/logo.jpg', 0, 'attachment', 'image/jpeg', 0),
(6, 1, '2016-03-30 17:18:38', '2016-03-30 17:18:38', '', 'favicon', '', 'inherit', 'open', 'closed', '', 'favicon', '', '', '2016-03-30 17:18:38', '2016-03-30 17:18:38', '', 0, 'http://onefocusdigital.com/wp-content/uploads/2016/03/favicon.png', 0, 'attachment', 'image/png', 0),
(7, 1, '2016-03-30 21:17:51', '2016-03-30 21:17:51', '', 'banner', '', 'inherit', 'open', 'closed', '', 'banner', '', '', '2016-03-30 21:17:51', '2016-03-30 21:17:51', '', 0, 'http://onefocusdigital.com/wp-content/uploads/2016/03/banner.jpg', 0, 'attachment', 'image/jpeg', 0),
(8, 1, '2016-03-30 21:57:49', '2016-03-30 21:57:49', '', 'idea', '', 'inherit', 'open', 'closed', '', 'idea', '', '', '2016-05-10 12:34:12', '2016-05-10 12:34:12', '', 0, 'http://onefocusdigital.com/wp-content/uploads/2016/03/idea.jpg', 0, 'attachment', 'image/jpeg', 0),
(9, 1, '2016-03-30 22:32:17', '0000-00-00 00:00:00', '', 'DARENVIOS', '', 'draft', 'closed', 'closed', '', '', '', '', '2016-03-30 22:32:17', '2016-03-30 22:32:17', '', 0, 'http://onefocusdigital.com/?post_type=otw-portfolio&#038;p=9', 0, 'otw-portfolio', '', 0),
(10, 1, '2016-03-30 22:42:45', '2016-03-30 22:42:45', '<h3>Descripción del Proyecto</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles del Proyecto</h3>\r\n<ul>\r\n	<li>Lorem Ipsum</li>\r\n	<li>Dolor Sit Amet</li>\r\n	<li>Consectetur</li>\r\n	<li>Adipiscing Elit</li>\r\n</ul>', 'DARENVIOS', '', 'publish', 'open', 'closed', '', 'darenvios', '', '', '2016-05-03 05:03:24', '2016-05-03 05:03:24', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=10', 0, 'portfolio', '', 0),
(11, 1, '2016-03-30 22:42:34', '2016-03-30 22:42:34', '', 'work1', '', 'inherit', 'open', 'closed', '', 'work1', '', '', '2016-03-30 22:42:34', '2016-03-30 22:42:34', '', 10, 'http://onefocusdigital.com/wp-content/uploads/2016/03/work1.jpg', 0, 'attachment', 'image/jpeg', 0),
(12, 1, '2016-03-30 22:42:45', '2016-03-30 22:42:45', 'Descripción', 'DARENVIOS', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2016-03-30 22:42:45', '2016-03-30 22:42:45', '', 10, 'http://onefocusdigital.com/2016/03/30/10-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2016-03-30 23:04:58', '2016-03-30 23:04:58', '<h3>Descripción del Proyecto</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles del Proyecto</h3>\r\n<ul>\r\n	<li>Lorem Ipsum</li>\r\n	<li>Dolor Sit Amet</li>\r\n	<li>Consectetur</li>\r\n	<li>Adipiscing Elit</li>\r\n</ul>', 'SECURITY & SOUND', '', 'publish', 'open', 'closed', '', 'security-sound', '', '', '2016-05-03 05:03:13', '2016-05-03 05:03:13', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=14', 0, 'portfolio', '', 0),
(15, 1, '2016-03-30 23:04:51', '2016-03-30 23:04:51', '', 'work2', '', 'inherit', 'open', 'closed', '', 'work2', '', '', '2016-03-30 23:04:51', '2016-03-30 23:04:51', '', 14, 'http://onefocusdigital.com/wp-content/uploads/2016/03/work2.jpg', 0, 'attachment', 'image/jpeg', 0),
(16, 1, '2016-03-30 23:04:58', '2016-03-30 23:04:58', '', 'SECURITY & SOUND', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2016-03-30 23:04:58', '2016-03-30 23:04:58', '', 14, 'http://onefocusdigital.com/2016/03/30/14-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2016-03-30 23:06:20', '2016-03-30 23:06:20', '<h3>Descripción del Proyecto</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles del Proyecto</h3>\r\n<ul>\r\n	<li>Lorem Ipsum</li>\r\n	<li>Dolor Sit Amet</li>\r\n	<li>Consectetur</li>\r\n	<li>Adipiscing Elit</li>\r\n</ul>', 'GBFSOURCE', '', 'publish', 'open', 'closed', '', 'gbfsource', '', '', '2016-05-03 05:02:53', '2016-05-03 05:02:53', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=17', 0, 'portfolio', '', 0),
(18, 1, '2016-03-30 23:05:49', '2016-03-30 23:05:49', '', 'work3', '', 'inherit', 'open', 'closed', '', 'work3', '', '', '2016-03-30 23:05:49', '2016-03-30 23:05:49', '', 17, 'http://onefocusdigital.com/wp-content/uploads/2016/03/work3.jpg', 0, 'attachment', 'image/jpeg', 0),
(19, 1, '2016-03-30 23:06:20', '2016-03-30 23:06:20', '', 'GBFSOURCE', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-03-30 23:06:20', '2016-03-30 23:06:20', '', 17, 'http://onefocusdigital.com/2016/03/30/17-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2016-03-30 23:24:08', '2016-03-30 23:24:08', '', 'GBFSOURCEaaaaa', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-03-30 23:24:08', '2016-03-30 23:24:08', '', 17, 'http://onefocusdigital.com/2016/03/30/17-revision-v1/', 0, 'revision', '', 0),
(21, 1, '2016-03-30 23:24:16', '2016-03-30 23:24:16', '', 'GBFSOURCE', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-03-30 23:24:16', '2016-03-30 23:24:16', '', 17, 'http://onefocusdigital.com/2016/03/30/17-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2016-04-01 19:09:59', '2016-04-01 19:09:59', '', 'GBFSOURCE asdasdasd', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-04-01 19:09:59', '2016-04-01 19:09:59', '', 17, 'http://onefocusdigital.com/2016/04/01/17-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2016-04-28 23:28:36', '2016-04-28 23:28:36', '', 'GBFSOURCE', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-04-28 23:28:36', '2016-04-28 23:28:36', '', 17, 'http://onefocusdigital.com/?p=31', 0, 'revision', '', 0),
(32, 1, '2016-04-28 23:31:37', '2016-04-28 23:31:37', '<p style="text-align: center;"><strong>MOISES<img class="alignnone size-medium wp-image-18" src="http://onefocusdigital.com/wp-content/uploads/2016/03/work3-300x280.jpg" alt="work3" width="300" height="280" /></strong><!--more--></p>\r\n&nbsp;', '', '', 'trash', 'open', 'closed', '', '32', '', '', '2016-04-28 23:35:52', '2016-04-28 23:35:52', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=32', 0, 'portfolio', '', 0),
(33, 1, '2016-04-28 23:29:27', '2016-04-28 23:29:27', '<ul>\r\ndfsf</ul>\r\n\r\n', '', '', 'inherit', 'closed', 'closed', '', '32-revision-v1', '', '', '2016-04-28 23:29:27', '2016-04-28 23:29:27', '', 32, 'http://onefocusdigital.com/?p=33', 0, 'revision', '', 0),
(34, 1, '2016-04-28 23:30:02', '2016-04-28 23:30:02', '<ul>\r\ndfsf</ul>\r\n\r\n<!--more-->\r\n\r\n<ins datetime="2016-04-28T23:29:15+00:00"><ul>\r\n	<li><code></code><code></code><code></code></li>\r\n</ul>\r\n\r\n</ins>', '', '', 'inherit', 'closed', 'closed', '', '32-revision-v1', '', '', '2016-04-28 23:30:02', '2016-04-28 23:30:02', '', 32, 'http://onefocusdigital.com/?p=34', 0, 'revision', '', 0),
(35, 1, '2016-04-28 23:30:44', '2016-04-28 23:30:44', '<strong>MOISES</strong><!--more-->\r\n\r\n&nbsp;', '', '', 'inherit', 'closed', 'closed', '', '32-revision-v1', '', '', '2016-04-28 23:30:44', '2016-04-28 23:30:44', '', 32, 'http://onefocusdigital.com/?p=35', 0, 'revision', '', 0),
(36, 1, '2016-04-28 23:31:37', '2016-04-28 23:31:37', '<p style="text-align: center;"><strong>MOISES<img class="alignnone size-medium wp-image-18" src="http://onefocusdigital.com/wp-content/uploads/2016/03/work3-300x280.jpg" alt="work3" width="300" height="280" /></strong><!--more--></p>\r\n&nbsp;', '', '', 'inherit', 'closed', 'closed', '', '32-revision-v1', '', '', '2016-04-28 23:31:37', '2016-04-28 23:31:37', '', 32, 'http://onefocusdigital.com/?p=36', 0, 'revision', '', 0),
(37, 1, '2016-04-28 23:34:29', '2016-04-28 23:34:29', '', '', '', 'inherit', 'closed', 'closed', '', '32-autosave-v1', '', '', '2016-04-28 23:34:29', '2016-04-28 23:34:29', '', 32, 'http://onefocusdigital.com/?p=37', 0, 'revision', '', 0),
(56, 1, '2016-05-03 02:37:24', '2016-05-03 02:37:24', 'fdsfsdfsdfsdfsdfsdfsdf', 'GBFSOURCE', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-05-03 02:37:24', '2016-05-03 02:37:24', '', 17, 'http://onefocusdigital.com/17-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2016-05-03 03:52:28', '2016-05-03 03:52:28', '<span style="color: #ff9900;"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, </span>', 'GBFSOURCE', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-05-03 03:52:28', '2016-05-03 03:52:28', '', 17, 'http://onefocusdigital.com/17-revision-v1/', 0, 'revision', '', 0),
(58, 1, '2016-05-03 03:57:37', '2016-05-03 03:57:37', '<span style="color: #ff9900;"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.  </span>', 'GBFSOURCE', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-05-03 03:57:37', '2016-05-03 03:57:37', '', 17, 'http://onefocusdigital.com/17-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2016-05-03 03:58:20', '2016-05-03 03:58:20', '<span style="color: #ff9900;"><span style="color: #ffffff;"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. </span> </span>', 'GBFSOURCE', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-05-03 03:58:20', '2016-05-03 03:58:20', '', 17, 'http://onefocusdigital.com/17-revision-v1/', 0, 'revision', '', 0),
(60, 1, '2016-05-03 03:59:04', '2016-05-03 03:59:04', '<p style="text-align: justify;"><span style="color: #ff9900;"><span style="color: #ffffff;"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. </span> </span></p>', 'GBFSOURCE', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-05-03 03:59:04', '2016-05-03 03:59:04', '', 17, 'http://onefocusdigital.com/17-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2016-05-03 03:59:25', '2016-05-03 03:59:25', '<p style="text-align: justify;"><span style="color: #ff9900;"><span style="color: #ffffff;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. </span> </span></p>', 'GBFSOURCE', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-05-03 03:59:25', '2016-05-03 03:59:25', '', 17, 'http://onefocusdigital.com/17-revision-v1/', 0, 'revision', '', 0),
(62, 1, '2016-05-03 04:29:18', '2016-05-03 04:29:18', '<h3>Project Description</h3>\n                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\n                <h3>Project Details</h3>\n                <ul>\n                    <li>Lorem Ipsum</li>\n                    <li>Dolor Sit Amet</li>\n                    <li>Consectetur</li>\n                    <li>Adipiscing Elit</li>\n                </ul>\n            </div>\n<p style="text-align: justify;"><strong><span style="color: #ff9900;"><span style="color: #ffffff;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. </span> </span></strong></p>', 'GBFSOURCE', '', 'inherit', 'closed', 'closed', '', '17-autosave-v1', '', '', '2016-05-03 04:29:18', '2016-05-03 04:29:18', '', 17, 'http://onefocusdigital.com/17-autosave-v1/', 0, 'revision', '', 0),
(63, 1, '2016-05-03 04:29:36', '2016-05-03 04:29:36', '<h3>Project Description</h3>\r\n                <p style="text-align: justify;"><strong><span style="color: #ff9900;"><span style="color: #ffffff;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n                <h3>Project Details</h3>\r\n                <ul>\r\n                    <li>Lorem Ipsum</li>\r\n                    <li>Dolor Sit Amet</li>\r\n                    <li>Consectetur</li>\r\n                    <li>Adipiscing Elit</li>\r\n                </ul>', 'GBFSOURCE', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-05-03 04:29:36', '2016-05-03 04:29:36', '', 17, 'http://onefocusdigital.com/17-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2016-05-03 04:56:46', '2016-05-03 04:56:46', '', 'cdsfaf', '', 'trash', 'open', 'closed', '', 'cdsfaf', '', '', '2016-05-03 05:01:25', '2016-05-03 05:01:25', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=64', 0, 'portfolio', '', 0),
(65, 1, '2016-05-03 05:01:25', '2016-05-03 05:01:25', '', 'cdsfaf', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2016-05-03 05:01:25', '2016-05-03 05:01:25', '', 64, 'http://onefocusdigital.com/64-revision-v1/', 0, 'revision', '', 0),
(66, 1, '2016-05-03 05:01:58', '2016-05-03 05:01:58', '<h3>Project Description</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Project Details</h3>\r\n<ul>\r\n	<li>Lorem Ipsum</li>\r\n	<li>Dolor Sit Amet</li>\r\n	<li>Consectetur</li>\r\n	<li>Adipiscing Elit</li>\r\n</ul>', 'GBFSOURCE', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-05-03 05:01:58', '2016-05-03 05:01:58', '', 17, 'http://onefocusdigital.com/17-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2016-05-03 05:02:53', '2016-05-03 05:02:53', '<h3>Descripción del Proyecto</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles del Proyecto</h3>\r\n<ul>\r\n	<li>Lorem Ipsum</li>\r\n	<li>Dolor Sit Amet</li>\r\n	<li>Consectetur</li>\r\n	<li>Adipiscing Elit</li>\r\n</ul>', 'GBFSOURCE', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-05-03 05:02:53', '2016-05-03 05:02:53', '', 17, 'http://onefocusdigital.com/17-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2016-05-03 05:03:13', '2016-05-03 05:03:13', '<h3>Descripción del Proyecto</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles del Proyecto</h3>\r\n<ul>\r\n	<li>Lorem Ipsum</li>\r\n	<li>Dolor Sit Amet</li>\r\n	<li>Consectetur</li>\r\n	<li>Adipiscing Elit</li>\r\n</ul>', 'SECURITY & SOUND', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2016-05-03 05:03:13', '2016-05-03 05:03:13', '', 14, 'http://onefocusdigital.com/14-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2016-05-03 05:03:24', '2016-05-03 05:03:24', '<h3>Descripción del Proyecto</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles del Proyecto</h3>\r\n<ul>\r\n	<li>Lorem Ipsum</li>\r\n	<li>Dolor Sit Amet</li>\r\n	<li>Consectetur</li>\r\n	<li>Adipiscing Elit</li>\r\n</ul>', 'DARENVIOS', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2016-05-03 05:03:24', '2016-05-03 05:03:24', '', 10, 'http://onefocusdigital.com/10-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2016-05-04 14:25:21', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2016-05-04 14:25:21', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?p=72', 0, 'post', '', 0),
(73, 1, '2016-05-04 14:29:55', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2016-05-04 14:29:55', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?p=73', 0, 'post', '', 0),
(74, 1, '2016-05-04 14:32:41', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-05-04 14:32:41', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?page_id=74', 0, 'page', '', 0),
(75, 1, '2016-05-04 14:34:06', '2016-05-04 14:34:06', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>Términos</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n</div>\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>Condiciones</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n<ol>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ol>\r\n</div>', 'Términos & Condiciones', '', 'publish', 'closed', 'closed', '', 'terminso-condiciones', '', '', '2016-05-04 16:58:45', '2016-05-04 16:58:45', '', 0, 'http://onefocusdigital.com/?page_id=75', 0, 'page', '', 0),
(76, 1, '2016-05-04 14:34:06', '2016-05-04 14:34:06', '', '', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 14:34:06', '2016-05-04 14:34:06', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(77, 1, '2016-05-04 14:37:59', '2016-05-04 14:37:59', '', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 14:37:59', '2016-05-04 14:37:59', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(78, 1, '2016-05-04 14:38:26', '2016-05-04 14:38:26', 'hola', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 14:38:26', '2016-05-04 14:38:26', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(79, 1, '2016-05-04 16:54:27', '2016-05-04 16:54:27', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-warnig ">\n<h3>Términos</h3>\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\n\n</div>\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-primary ">\n<h3>Condiciones</h3>\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\n\n<ol>\n 	<li>Lorem Ipsum</li>\n 	<li>Dolor Sit Amet</li>\n 	<li>Consectetur</li>\n 	<li>Adipiscing Elit</li>\n</ol>\n</div>', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-autosave-v1', '', '', '2016-05-04 16:54:27', '2016-05-04 16:54:27', '', 75, 'http://onefocusdigital.com/75-autosave-v1/', 0, 'revision', '', 0),
(80, 1, '2016-05-04 15:18:15', '2016-05-04 15:18:15', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n</div>\r\n\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus.</p> \r\n\r\n</div>\r\n\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n</div>\r\n', 'Preguntas Frecuentes', '', 'publish', 'closed', 'closed', '', 'preguntas-frecuentes', '', '', '2016-05-07 18:50:16', '2016-05-07 18:50:16', '', 0, 'http://onefocusdigital.com/?page_id=80', 0, 'page', '', 0),
(81, 1, '2016-05-04 15:18:15', '2016-05-04 15:18:15', 'hehehe', 'Preguntas Frecuentes', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2016-05-04 15:18:15', '2016-05-04 15:18:15', '', 80, 'http://onefocusdigital.com/80-revision-v1/', 0, 'revision', '', 0),
(82, 1, '2016-05-04 15:26:59', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2016-05-04 15:26:59', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&p=82', 0, 'portfolio', '', 0),
(83, 1, '2016-05-04 15:28:05', '2016-05-04 15:28:05', '<h3>Descripción del Proyecto</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles del Proyecto</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 15:28:05', '2016-05-04 15:28:05', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(84, 1, '2016-05-04 15:29:38', '2016-05-04 15:29:38', '<h3>Nuestros Términos</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles de Terminos</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>\r\n<h3>Nuestras Condiciones</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles de Condiciones</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 15:29:38', '2016-05-04 15:29:38', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(85, 1, '2016-05-04 15:32:11', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-05-04 15:32:11', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?page_id=85', 0, 'page', '', 0),
(86, 1, '2016-05-04 17:00:16', '2016-05-04 17:00:16', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\n<h3>¿gravida pellentesque urna varius vitae?</h3>\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\n\n</div>\n\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\n<h3>¿gravida pellentesque urna varius vitae?</h3>\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\n\n</div>\n\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\n<h3>¿gravida pellentesque urna varius vitae?</h3>\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\n\n</div>\n\n', 'Preguntas Frecuentes', '', 'inherit', 'closed', 'closed', '', '80-autosave-v1', '', '', '2016-05-04 17:00:16', '2016-05-04 17:00:16', '', 80, 'http://onefocusdigital.com/80-autosave-v1/', 0, 'revision', '', 0),
(87, 1, '2016-05-04 15:33:06', '2016-05-04 15:33:06', '<h3>Preguntas Frecuentes</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles de Terminos</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>', 'Preguntas Frecuentes', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2016-05-04 15:33:06', '2016-05-04 15:33:06', '', 80, 'http://onefocusdigital.com/80-revision-v1/', 0, 'revision', '', 0),
(88, 1, '2016-05-04 15:44:58', '2016-05-04 15:44:58', '<div class="bs-callout bs-callout-danger" id="callout-navbar-fixed-top-padding">\r\n	\r\n\r\n<h3>Nuestros Términos</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles de Terminos</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>\r\n</div>\r\n<h3>Nuestras Condiciones</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles de Condiciones</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 15:44:58', '2016-05-04 15:44:58', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(89, 1, '2016-05-04 16:04:13', '2016-05-04 16:04:13', '<div class="bs-callout bs-callout-theme" id="callout-navbar-fixed-top-padding">\r\n	\r\n\r\n<h3>Nuestros Términos</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles de Terminos</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>\r\n</div>\r\n<h3>Nuestras Condiciones</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles de Condiciones</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 16:04:13', '2016-05-04 16:04:13', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2016-05-04 16:14:17', '2016-05-04 16:14:17', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme">\r\n<h3>Nuestros Términos</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles de Terminos</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>\r\n</div>\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme">\r\n<h3>Nuestras Condiciones</h3>\r\n<p style="text-align: justify;"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</strong></p>\r\n\r\n<h3>Detalles de Condiciones</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>\r\n</div>', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 16:14:17', '2016-05-04 16:14:17', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(91, 1, '2016-05-04 16:29:45', '2016-05-04 16:29:45', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme">\r\n<h3>Nuestros Términos</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n<h3>Detalles de Terminos</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>\r\n</div>\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme">\r\n<h3>Nuestras Condiciones</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n<h3>Detalles de Condiciones</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>\r\n</div>', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 16:29:45', '2016-05-04 16:29:45', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2016-05-04 16:38:01', '2016-05-04 16:38:01', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme">\r\n<h3>Términos</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n<h3>Detalles</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>\r\n</div>\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme">\r\n<h3>Condiciones</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n<h3>Detalles</h3>\r\n<ul>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ul>\r\n</div>', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 16:38:01', '2016-05-04 16:38:01', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(93, 1, '2016-05-04 16:40:20', '2016-05-04 16:40:20', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme">\r\n<h3>Términos</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n</div>\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme">\r\n<h3>Condiciones</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n<ol>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ol>\r\n</div>', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 16:40:20', '2016-05-04 16:40:20', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(94, 1, '2016-05-04 16:40:44', '2016-05-04 16:40:44', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme">\r\n<h3>Términos</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n</div>\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme">\r\n<h3>Condiciones</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n<ol>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ol>\r\n</div>', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 16:40:44', '2016-05-04 16:40:44', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(95, 1, '2016-05-04 16:51:25', '2016-05-04 16:51:25', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-primary ">\r\n<h3>Términos</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n</div>\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-primary ">\r\n<h3>Condiciones</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n<ol>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ol>\r\n</div>', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 16:51:25', '2016-05-04 16:51:25', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2016-05-04 16:54:30', '2016-05-04 16:54:30', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-warnig ">\r\n<h3>Términos</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n</div>\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-warnig ">\r\n<h3>Condiciones</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n<ol>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ol>\r\n</div>', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 16:54:30', '2016-05-04 16:54:30', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(97, 1, '2016-05-04 16:55:46', '2016-05-04 16:55:46', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>Términos</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n</div>\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>Condiciones</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n<ol>\r\n 	<li>Lorem Ipsum</li>\r\n 	<li>Dolor Sit Amet</li>\r\n 	<li>Consectetur</li>\r\n 	<li>Adipiscing Elit</li>\r\n</ol>\r\n</div>', 'Términos & Condiciones', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2016-05-04 16:55:46', '2016-05-04 16:55:46', '', 75, 'http://onefocusdigital.com/75-revision-v1/', 0, 'revision', '', 0),
(98, 1, '2016-05-04 16:59:47', '2016-05-04 16:59:47', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n</div>\r\n\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n</div>\r\n\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n</div>\r\n\r\n', 'Preguntas Frecuentes', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2016-05-04 16:59:47', '2016-05-04 16:59:47', '', 80, 'http://onefocusdigital.com/80-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(99, 1, '2016-05-04 17:00:42', '2016-05-04 17:00:42', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n</div>\r\n\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus.</p> \r\n\r\n</div>\r\n\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n</div>\r\n\r\n', 'Preguntas Frecuentes', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2016-05-04 17:00:42', '2016-05-04 17:00:42', '', 80, 'http://onefocusdigital.com/80-revision-v1/', 0, 'revision', '', 0),
(100, 1, '2016-05-04 18:00:48', '2016-05-04 18:00:48', '<div class="form-group col-md-6">\r\n	<label>Nombre</label>\r\n        [text* name class:form-control  placeholder "Nombre"]\r\n</div>\r\n\r\n<div class="form-group col-md-6">\r\n	<label>Apellido</label>\r\n        [text* last_name class:form-control  placeholder "Apellido"]\r\n</div>\r\n\r\n<div class="form-group col-md-6">\r\n	<label>Teléfono</label>\r\n        [tel* phone_contact class:form-control  placeholder "Telefono"]\r\n</div>\r\n\r\n<div class="form-group col-md-6">\r\n	<label>Email</label>\r\n        [email* email class:form-control  placeholder "Email"]\r\n</div>\r\n\r\n<div class="form-group col-md-12">\r\n	<label>¿Como escucho sobre nosotros?</label>\r\n        [select* reference class:form-control "" "Recomendado por alguien" "Televisión" "Radio" "Periodico" "Google"]\r\n</div>\r\n\r\n<div class="form-group col-md-12">\r\n	<label>Describenos tu idea</label>\r\n        [textarea* describe_your_idea 40x2  class:form-control placeholder "Tu idea nose importa..."]\r\n</div>\r\n<div class="form-group col-md-12 quiz-style">\r\n\r\n <div class="input-group">\r\n  <div class="input-group-addon">3 + 1</div>\r\n  [quiz quiz-618   class:form-control "|4"]\r\n </div>\r\n</div>\r\n\r\n<div class="col-md-12 form-group ">\r\n        [submit class:boton-enviar class:form-control "Enviar"]\r\n</div>\nOneFocusDigital "[your-subject]"\n[your-name] <irwing@onefocusdigital.com>\nDe: [your-name] <[your-email]>\r\nAsunto: [your-subject]\r\n\r\nCuerpo del mensaje:\r\n[your-message]\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en OneFocusDigital (http://onefocusdigital.com)\nirwing@onefocusdigital.com\nReply-To: [email]\n\n\n\n\nOneFocusDigital "[your-subject]"\nOneFocusDigital <irwing@onefocusdigital.com>\nCuerpo del mensaje:\r\n[your-message]\r\n\r\n--\r\nEste mensaje se ha enviado desde un formulario de contacto en OneFocusDigital (http://onefocusdigital.com)\n[your-email]\nReply-To: irwing@onefocusdigital.com\n\n\n\nGracias por tu mensaje. Ha sido enviado.\nHubo un error intentando enviar tu mensaje. Por favor inténtalo de nuevo más tarde.\nUno o más campos tienen un error. Por favor revisa e inténtalo de nuevo.\nHubo un error intentando enviar tu mensaje. Por favor inténtalo de nuevo más tarde.\nDebes aceptar los términos y condiciones antes de enviar tu mensaje.\nEl campo es obligatorio.\nEl campo es demasiado largo.\nEl campo es demasiado corto.\nEl formato de fecha es incorrecto.\nLa fecha es anterior a la más temprana permitida.\nLa fecha es posterior a la más tardía permitida.\nHubo un error desconocido subiendo el archivo.\nNo tienes permisos para subir archivos de este tipo.\nEl archivo es demasiado grande.\nSe ha producido un error subiendo la imagen\nEl formato de número no es válido.\nEl número es menor que el mínimo permitido.\nEl número es mayor que el máximo permitido.\nLa respuesta al cuestionario no es correcta.\nEl código introducido es incorrecto.\nLa dirección de correo electrónico que has introducido no es válida.\nLa URL no es válida.\nEl número de teléfono no es válido.', 'Formulario de contacto 1', '', 'publish', 'closed', 'closed', '', 'formulario-de-contacto-1', '', '', '2016-05-07 16:44:54', '2016-05-07 16:44:54', '', 0, 'http://onefocusdigital.com/?post_type=wpcf7_contact_form&#038;p=100', 0, 'wpcf7_contact_form', '', 0),
(101, 1, '2016-05-04 19:11:46', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2016-05-04 19:11:46', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?p=101', 0, 'post', '', 0),
(104, 1, '2016-05-06 22:00:46', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2016-05-06 22:00:46', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?p=104', 0, 'post', '', 0),
(105, 1, '2016-05-07 17:47:06', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2016-05-07 17:47:06', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&p=105', 0, 'portfolio', '', 0),
(106, 1, '2016-05-07 17:58:19', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2016-05-07 17:58:19', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&p=106', 0, 'portfolio', '', 0),
(107, 1, '2016-05-07 18:09:17', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2016-05-07 18:09:17', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?p=107', 0, 'post', '', 0),
(108, 1, '2016-05-07 18:49:38', '2016-05-07 18:49:38', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n</div>\r\n\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus.</p> \r\n\r\n</div>\r\n\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n</div>\r\n[accordion_slider id="1"]\r\n', 'Preguntas Frecuentes', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2016-05-07 18:49:38', '2016-05-07 18:49:38', '', 80, 'http://onefocusdigital.com/80-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2016-05-07 18:50:16', '2016-05-07 18:50:16', '<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n</div>\r\n\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus.</p> \r\n\r\n</div>\r\n\r\n<div id="callout-navbar-fixed-top-padding" class="bs-callout bs-callout-theme ">\r\n<h3>¿gravida pellentesque urna varius vitae?</h3>\r\n<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>\r\n\r\n</div>\r\n', 'Preguntas Frecuentes', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2016-05-07 18:50:16', '2016-05-07 18:50:16', '', 80, 'http://onefocusdigital.com/80-revision-v1/', 0, 'revision', '', 0),
(112, 1, '2016-05-07 20:59:35', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-05-07 20:59:35', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?post_type=logoshowcase&p=112', 0, 'logoshowcase', '', 0),
(113, 1, '2016-05-07 20:59:40', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-05-07 20:59:40', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?post_type=logoshowcase&p=113', 0, 'logoshowcase', '', 0),
(119, 1, '2016-05-07 22:12:29', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2016-05-07 22:12:29', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?p=119', 0, 'post', '', 0),
(131, 1, '2016-05-07 23:36:45', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-05-07 23:36:45', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?post_type=slick_slider&p=131', 0, 'slick_slider', '', 0),
(133, 1, '2016-05-07 23:44:40', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-05-07 23:44:40', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?post_type=slick_slider&p=133', 0, 'slick_slider', '', 0),
(153, 1, '2016-05-10 12:46:48', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-05-10 12:46:48', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?post_type=slick_slider&p=153', 0, 'slick_slider', '', 0),
(157, 1, '2016-05-10 14:23:19', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2016-05-10 14:23:19', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&p=157', 0, 'portfolio', '', 0),
(158, 1, '2016-05-10 14:23:21', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2016-05-10 14:23:21', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&p=158', 0, 'portfolio', '', 0),
(159, 1, '2016-05-10 14:50:36', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2016-05-10 14:50:36', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&p=159', 0, 'portfolio', '', 0),
(160, 1, '2016-05-10 14:51:19', '2016-05-10 14:51:19', '', 'Prueba1', '', 'publish', 'open', 'closed', '', 'prueba1', '', '', '2016-05-10 15:30:59', '2016-05-10 15:30:59', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=160', 0, 'portfolio', '', 0),
(161, 1, '2016-05-10 14:51:19', '2016-05-10 14:51:19', '', 'lol', '', 'inherit', 'closed', 'closed', '', '160-revision-v1', '', '', '2016-05-10 14:51:19', '2016-05-10 14:51:19', '', 160, 'http://onefocusdigital.com/160-revision-v1/', 0, 'revision', '', 0),
(162, 1, '2016-05-10 15:30:59', '2016-05-10 15:30:59', '', 'Prueba1', '', 'inherit', 'closed', 'closed', '', '160-revision-v1', '', '', '2016-05-10 15:30:59', '2016-05-10 15:30:59', '', 160, 'http://onefocusdigital.com/160-revision-v1/', 0, 'revision', '', 0),
(163, 1, '2016-05-10 15:31:26', '0000-00-00 00:00:00', '', 'Prueba2', '', 'draft', 'open', 'closed', '', '', '', '', '2016-05-10 15:31:26', '2016-05-10 15:31:26', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=163', 0, 'portfolio', '', 0),
(164, 1, '2016-05-10 15:31:56', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2016-05-10 15:31:56', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&p=164', 0, 'portfolio', '', 0),
(165, 1, '2016-05-10 15:47:32', '2016-05-10 15:47:32', '', 'Prueba 2', '', 'publish', 'open', 'closed', '', 'prueba-2', '', '', '2016-05-10 15:47:32', '2016-05-10 15:47:32', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=165', 0, 'portfolio', '', 0),
(166, 1, '2016-05-10 15:47:32', '2016-05-10 15:47:32', '', 'Prueba 2', '', 'inherit', 'closed', 'closed', '', '165-revision-v1', '', '', '2016-05-10 15:47:32', '2016-05-10 15:47:32', '', 165, 'http://onefocusdigital.com/165-revision-v1/', 0, 'revision', '', 0),
(167, 1, '2016-05-10 15:48:00', '2016-05-10 15:48:00', '', 'Prueba 3', '', 'publish', 'open', 'closed', '', 'prueba-3', '', '', '2016-05-10 15:48:00', '2016-05-10 15:48:00', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=167', 0, 'portfolio', '', 0),
(168, 1, '2016-05-10 15:48:00', '2016-05-10 15:48:00', '', 'Prueba 3', '', 'inherit', 'closed', 'closed', '', '167-revision-v1', '', '', '2016-05-10 15:48:00', '2016-05-10 15:48:00', '', 167, 'http://onefocusdigital.com/167-revision-v1/', 0, 'revision', '', 0),
(169, 1, '2016-05-10 15:48:14', '2016-05-10 15:48:14', '', 'Prueba 4', '', 'publish', 'open', 'closed', '', 'prueba-4', '', '', '2016-05-10 15:48:14', '2016-05-10 15:48:14', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=169', 0, 'portfolio', '', 0),
(170, 1, '2016-05-10 15:48:14', '2016-05-10 15:48:14', '', 'Prueba 4', '', 'inherit', 'closed', 'closed', '', '169-revision-v1', '', '', '2016-05-10 15:48:14', '2016-05-10 15:48:14', '', 169, 'http://onefocusdigital.com/169-revision-v1/', 0, 'revision', '', 0),
(171, 1, '2016-05-10 15:48:34', '2016-05-10 15:48:34', '', 'Prueba 5', '', 'publish', 'open', 'closed', '', 'prueba-5', '', '', '2016-05-10 15:48:34', '2016-05-10 15:48:34', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=171', 0, 'portfolio', '', 0),
(172, 1, '2016-05-10 15:48:34', '2016-05-10 15:48:34', '', 'Prueba 5', '', 'inherit', 'closed', 'closed', '', '171-revision-v1', '', '', '2016-05-10 15:48:34', '2016-05-10 15:48:34', '', 171, 'http://onefocusdigital.com/171-revision-v1/', 0, 'revision', '', 0),
(173, 1, '2016-05-10 15:48:50', '2016-05-10 15:48:50', '', 'Prueba 6', '', 'publish', 'open', 'closed', '', 'prueba-6', '', '', '2016-05-10 16:20:38', '2016-05-10 16:20:38', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=173', 0, 'portfolio', '', 0),
(174, 1, '2016-05-10 15:48:50', '2016-05-10 15:48:50', '', 'Prueba 6', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2016-05-10 15:48:50', '2016-05-10 15:48:50', '', 173, 'http://onefocusdigital.com/173-revision-v1/', 0, 'revision', '', 0),
(175, 1, '2016-05-10 15:49:32', '2016-05-10 15:49:32', '', 'Prueba 7', '', 'publish', 'open', 'closed', '', 'prueba-7', '', '', '2016-05-10 16:21:22', '2016-05-10 16:21:22', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=175', 0, 'portfolio', '', 0),
(176, 1, '2016-05-10 15:49:32', '2016-05-10 15:49:32', '', 'Prueba 7', '', 'inherit', 'closed', 'closed', '', '175-revision-v1', '', '', '2016-05-10 15:49:32', '2016-05-10 15:49:32', '', 175, 'http://onefocusdigital.com/175-revision-v1/', 0, 'revision', '', 0),
(177, 1, '2016-05-10 16:35:45', '2016-05-10 16:35:45', '', 'Prueba 8', '', 'publish', 'open', 'closed', '', 'prueba-8', '', '', '2016-05-10 16:35:45', '2016-05-10 16:35:45', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=177', 0, 'portfolio', '', 0),
(178, 1, '2016-05-10 16:35:45', '2016-05-10 16:35:45', '', 'Prueba 8', '', 'inherit', 'closed', 'closed', '', '177-revision-v1', '', '', '2016-05-10 16:35:45', '2016-05-10 16:35:45', '', 177, 'http://onefocusdigital.com/177-revision-v1/', 0, 'revision', '', 0),
(179, 1, '2016-05-10 16:36:01', '2016-05-10 16:36:01', '', 'Prueba 9', '', 'publish', 'open', 'closed', '', 'prueba-9', '', '', '2016-05-10 16:36:01', '2016-05-10 16:36:01', '', 0, 'http://onefocusdigital.com/?post_type=portfolio&#038;p=179', 0, 'portfolio', '', 0),
(180, 1, '2016-05-10 16:36:01', '2016-05-10 16:36:01', '', 'Prueba 9', '', 'inherit', 'closed', 'closed', '', '179-revision-v1', '', '', '2016-05-10 16:36:01', '2016-05-10 16:36:01', '', 179, 'http://onefocusdigital.com/179-revision-v1/', 0, 'revision', '', 0),
(181, 1, '2016-05-11 12:34:48', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-05-11 12:34:48', '0000-00-00 00:00:00', '', 0, 'http://onefocusdigital.com/?post_type=slick_slider&p=181', 0, 'slick_slider', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE IF NOT EXISTS `wp_termmeta` (
`meta_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE IF NOT EXISTS `wp_terms` (
`term_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Sitio Web', 'sitio-web', 0),
(3, 'Diseño', 'diseno', 0),
(4, 'Ecommerce', 'ecommerce', 0),
(5, 'Diseño', 'diseno', 0),
(6, 'Website', 'website', 0),
(7, 'Marketing', 'marketing', 0),
(8, 'Proyectos', 'project', 0),
(9, 'Proyectos', 'project', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(10, 5, 0),
(14, 6, 0),
(17, 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
`term_taxonomy_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'otw-portfolio-category', '', 0, 0),
(3, 3, 'otw-portfolio-category', '', 0, 0),
(4, 4, 'otw-portfolio-category', '', 0, 0),
(5, 5, 'portfolio_category', '', 0, 1),
(6, 6, 'portfolio_category', '', 0, 2),
(7, 7, 'portfolio_category', '', 0, 0),
(8, 8, 'wplss_logo_showcase_cat', 'sdfsdf', 0, 0),
(9, 9, 'wpsisac_slider-category', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE IF NOT EXISTS `wp_usermeta` (
`umeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'wp_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', ''),
(13, 1, 'show_welcome_panel', '1'),
(14, 1, 'session_tokens', 'a:1:{s:64:"582171d46dde2efbaacd621b04634c921144aff3f45c1a42006d12fd73f6f47e";a:4:{s:10:"expiration";i:1463055327;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:83:"Mozilla/5.0 (X11; Linux i686; rv:38.0) Gecko/20100101 Firefox/38.0 Iceweasel/38.5.0";s:5:"login";i:1462882527;}}'),
(15, 1, 'wp_dashboard_quick_press_last_post_id', '104'),
(16, 1, 'wp_user-settings', 'libraryContent=browse&editor=html&post_dfw=off&hidetb=1'),
(17, 1, 'wp_user-settings-time', '1462885075'),
(18, 1, 'closedpostboxes_otw-portfolio', 'a:0:{}'),
(19, 1, 'metaboxhidden_otw-portfolio', 'a:1:{i:0;s:7:"slugdiv";}'),
(20, 1, 'closedpostboxes_dashboard', 'a:4:{i:0;s:19:"dashboard_right_now";i:1;s:18:"dashboard_activity";i:2;s:21:"dashboard_quick_press";i:3;s:17:"dashboard_primary";}'),
(21, 1, 'metaboxhidden_dashboard', 'a:0:{}'),
(22, 1, 'closedpostboxes_portfolio', 'a:0:{}'),
(23, 1, 'metaboxhidden_portfolio', 'a:1:{i:0;s:7:"slugdiv";}'),
(24, 1, 'meta-box-order_page', 'a:3:{s:4:"side";s:36:"submitdiv,pageparentdiv,postimagediv";s:6:"normal";s:70:"revisionsdiv,postcustom,commentstatusdiv,commentsdiv,slugdiv,authordiv";s:8:"advanced";s:0:"";}'),
(25, 1, 'screen_layout_page', '2'),
(26, 1, 'closedpostboxes_page', 'a:0:{}'),
(27, 1, 'metaboxhidden_page', 'a:6:{i:0;s:12:"revisionsdiv";i:1;s:10:"postcustom";i:2;s:16:"commentstatusdiv";i:3;s:11:"commentsdiv";i:4;s:7:"slugdiv";i:5;s:9:"authordiv";}'),
(28, 1, 'closedpostboxes_logoshowcase', 'a:0:{}'),
(29, 1, 'metaboxhidden_logoshowcase', 'a:1:{i:0;s:7:"slugdiv";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE IF NOT EXISTS `wp_users` (
`ID` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BbvZYsul98qX/98YAdKKagP1O6Q0J2/', 'admin', 'irwing@onefocusdigital.com', '', '2016-03-29 20:42:28', '', 0, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
 ADD PRIMARY KEY (`meta_id`), ADD KEY `comment_id` (`comment_id`), ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
 ADD PRIMARY KEY (`comment_ID`), ADD KEY `comment_post_ID` (`comment_post_ID`), ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`), ADD KEY `comment_date_gmt` (`comment_date_gmt`), ADD KEY `comment_parent` (`comment_parent`), ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_crp_portfolios`
--
ALTER TABLE `wp_crp_portfolios`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_crp_projects`
--
ALTER TABLE `wp_crp_projects`
 ADD PRIMARY KEY (`id`), ADD KEY `crp_pid_fk` (`pid`);

--
-- Indexes for table `wp_huge_itportfolio_images`
--
ALTER TABLE `wp_huge_itportfolio_images`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_huge_itportfolio_portfolios`
--
ALTER TABLE `wp_huge_itportfolio_portfolios`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
 ADD PRIMARY KEY (`link_id`), ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
 ADD PRIMARY KEY (`option_id`), ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
 ADD PRIMARY KEY (`meta_id`), ADD KEY `post_id` (`post_id`), ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
 ADD PRIMARY KEY (`ID`), ADD KEY `post_name` (`post_name`(191)), ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`), ADD KEY `post_parent` (`post_parent`), ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
 ADD PRIMARY KEY (`meta_id`), ADD KEY `term_id` (`term_id`), ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
 ADD PRIMARY KEY (`term_id`), ADD KEY `slug` (`slug`(191)), ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
 ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`), ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
 ADD PRIMARY KEY (`term_taxonomy_id`), ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`), ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
 ADD PRIMARY KEY (`umeta_id`), ADD KEY `user_id` (`user_id`), ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
 ADD PRIMARY KEY (`ID`), ADD KEY `user_login_key` (`user_login`), ADD KEY `user_nicename` (`user_nicename`), ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
MODIFY `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_crp_portfolios`
--
ALTER TABLE `wp_crp_portfolios`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_crp_projects`
--
ALTER TABLE `wp_crp_projects`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_huge_itportfolio_images`
--
ALTER TABLE `wp_huge_itportfolio_images`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `wp_huge_itportfolio_portfolios`
--
ALTER TABLE `wp_huge_itportfolio_portfolios`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
MODIFY `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
MODIFY `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=567;
--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=362;
--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=182;
--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
MODIFY `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
MODIFY `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
MODIFY `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `wp_crp_projects`
--
ALTER TABLE `wp_crp_projects`
ADD CONSTRAINT `crp_pid_fk` FOREIGN KEY (`pid`) REFERENCES `wp_crp_portfolios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
